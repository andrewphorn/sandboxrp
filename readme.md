### How to install
Place contents of this directory (sandboxrp) into a folder named `sandboxrp` in
the `gamemodes` folder.

### Settings
Open rpsettings.txt (may be named rpsettings2.txt etc) file in data/sandboxrp/ and change values from 2nd table.
You can find explanation in sandboxrp/gamemode/configuration.lua file.


### Blocked props/tools
To add / remove blocked props go to the data/sandboxrp/settings/blockedprops.txt file and 
add / remove blocked props. You can enter a part of the name or the whole name.
To add / remove blocked tools do the same thing as with blocked props but the blocked tools
file is data/GmodRP/settings/blockedtools.txt and you need to use the whole tool's name.


### Unownable doors

You must be admin or superadmin to add/remove ownable doors.

To add unownable doors, load the map you want to add unowable doors on, then aim to the door
you want to be unownable, and execute this console command: gm_rp_door_unownable

To remove a door from the 'unownable' list, look at the door you want to be ownable and execute 
this console command: gm_rp_door_ownable

Have fun roleplaying!

Gamemode originally by Slob187.

Updated & Modified by AndrewPH.