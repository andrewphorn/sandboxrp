AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

/*---------------------------------------------------------
   Name: Initialize
---------------------------------------------------------*/
function ENT:Initialize()
	self:SetModel( "models/props_c17/consolebox01a.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self.SparkStart = 0
	self.Sparking = false
	self.HP = 125
	self.Ignite_Chance = 1 -- percent
	self.Min_Print_Time = 30
	self.Max_Print_Time = 150
	self.Print_Amount = 400
	self.NextMoneyTime = os.time() + math.Rand(self.Min_Print_Time, self.Max_Print_Time)
end