 	
ENT.Type 		= "anim"

ENT.PrintName	= ""
ENT.Author		= ""
ENT.Contact		= ""


AddCSLuaFile( "shared.lua" )


/*---------------------------------------------------------
   Name: Initialize
---------------------------------------------------------*/
function ENT:Initialize()

	if ( SERVER ) then
	
		self:SetModel( "models/props_wasteland/dockplank01b.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self.alpha = 255
		self.removeTime = 0
		self.used = false
		
	end

end

/*---------------------------------------------------------
   Name: Initialize
---------------------------------------------------------*/
function ENT:Think()

	if ( SERVER ) then
	
		if ( self.used ) then
		
			if ( CurTime() > self.removeTime ) then
			
				if ( self.alpha <= 0 ) then self:Remove() end
				
				self.alpha = self.alpha - 10
				
				self:SetColor( Color(255, 255, 255, self.alpha) )
			
				self.removeTime = CurTime() + .1
				
			end

		end
	
	end

end


function ENT:Touch( hitEnt )

	if (SERVER) then

		if ( hitEnt:GetVelocity():Length() > 100 ) && ( string.find( hitEnt:GetClass(), "vehicle" ) ) then
		
			if hitEnt:GetDriver():IsValid() then
			
				hitEnt:GetDriver():ExitVehicle()
				self.used = true
				
			end

		end
	
	end
	
end