AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

/*---------------------------------------------------------
   Name: Initialize
---------------------------------------------------------*/
function ENT:Initialize()
	self:SetModel( "models/props_c17/consolebox01a.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self.SparkStart = 0
	self.Sparking = false
	self.HP = 100
	self.Ignite_Chance = 5 -- percent
	self.Min_Print_Time = 60
	self.Max_Print_Time = 180
	self.Print_Amount = 200
	self.NextMoneyTime = os.time() + math.Rand(self.Min_Print_Time, self.Max_Print_Time)
end

/*---------------------------------------------------------
   Name: Think
---------------------------------------------------------*/
function ENT:Think()

	if os.time() > self.NextMoneyTime then
		-- Start sparking!
		self.NextMoneyTime = os.time() + math.Rand(self.Min_Print_Time, self.Max_Print_Time)
		self.SparkStart = os.time()
		self.Sparking = true
	end

	if self.Sparking then
		if self.SparkStart + 5 < os.time() then
			-- Sparking is over, create some cash
			self:CreateMoney()

			self.Sparking = false
		elseif self.SparkStart + 5 > os.time() then
			-- we are still sparking, make some sparks matey!
			local sPos = self:GetPos()
			local spark = EffectData()
			spark:SetOrigin(sPos)
			spark:SetMagnitude(1)
			spark:SetScale(1.5)
			spark:SetRadius(2)
			util.Effect("Sparks", spark)
		end
	end

end

function ENT:OnTakeDamage(amount)
	amount = amount:GetDamage()

	self.HP = math.max(0, self.HP - amount)
	if self.HP == 0 then
		self:Explode()
	elseif self.HP < 25 and !self:IsOnFire() then
		self:CatchFire()
	end
end

function ENT:CatchFire()
	if self:IsOnFire() then return end

	self:Ignite(60)
	local owner = self:GetEntOwner()
	if owner and IsValid(owner) and owner:IsPlayer() then
		owner:RedNotify("One of your money printers has caught fire!")
	end
end

function ENT:CreateMoney(amount)
	amount = amount or self.Print_Amount

	if self:IsOnFire() then return end

	local owner = self:GetEntOwner()
	if owner and IsValid(owner) and owner:IsPlayer() then
		if math.Rand(1,100) <= self.Ignite_Chance then
			self:CatchFire()
			return
		else
			owner:GreenNotify("One of your money printers has finished printing!")
		end
	end

	for k, ent in pairs(ents.FindInSphere(self:GetPos(),40)) do
		if ent.Money then 
			ent.Amount = ent.Amount + amount
			ent:SetNW2Int("moneyAmount", ent.Amount)
			return
		end
	end

	local basepos = self:GetPos()
	local money = ents.Create("prop_physics")
	money:SetModel( "models/weapons/w_suitcase_passenger.mdl" )
	money:SetPos( Vector(basepos.x + 10, basepos.y, basepos.z + 25) )
	money:Spawn()
	money:Activate()
	money:GetPhysicsObject():Wake()
	money.Money = true
	money.Amount = amount
	money:SetNW2Bool("isMoney", true)
	money:SetNW2Int("moneyAmount", money.Amount)
end

function ENT:Explode()
	local ePos = self:GetPos()
	local explosion = EffectData()
	explosion:SetStart(ePos)
	explosion:SetOrigin(ePos)
	explosion:SetScale(1.2)
	util.Effect("Explosion", explosion)
	self:Remove()

	local owner = self:GetEntOwner()
	if owner and IsValid(owner) and owner:IsPlayer() then
		owner:RedNotify("One of your money printers has exploded!")
	end
end