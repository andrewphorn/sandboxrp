
// Variables that are used on both client and server

SWEP.Author		= "AndrewPH"
SWEP.Contact		= ""
SWEP.Purpose		= "Unlock doors with force."
SWEP.Instructions	= "Left click to ram doors."

SWEP.ViewModel		= "models/weapons/v_RPG.mdl"
SWEP.WorldModel		= "models/weapons/w_rocket_launcher.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextUse = 0


/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType( "rpg" )	
						
end
	

/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
	if SERVER then    
		if CurTime() < self.NextUse then return end

		local trace = {}
	    trace.start = self.Owner:EyePos()
		trace.endpos = trace.start + self.Owner:GetAimVector() * 50
		trace.filter = self.Owner
		local tr = util.TraceLine(trace) 

		if (IsValid(tr.Entity) && tr.Entity:isOwnable()) then
			local ent = tr.Entity
			ent:EmitSound("physics/wood/wood_crate_impact_hard1.wav")
			if math.random(1,100) > 35 then
				ent:EmitSound( "doors/door_latch3.wav" )
				ent:Fire( "unlock", "", 0 )
				ent:Fire( "open", "", 0 )
			end
		end
		
		self.NextUse = CurTime() + 2
	end

end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()

end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end