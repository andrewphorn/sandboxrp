
// Variables that are used on both client and server

SWEP.Author		= "Slob187"
SWEP.Contact		= "slob187.pb@gmail.com"
SWEP.Purpose		= "Handcuffs."
SWEP.Instructions	= "Left click to use."

SWEP.ViewModel		= "models/Weapons/V_hands.mdl"
SWEP.WorldModel		= "models/weapons/w_crowbar.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextUse = 0


/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType("normal")	

end
	
/*---------------------------------------------------------
	Deploy
---------------------------------------------------------*/
function SWEP:Deploy()

    if (!SERVER) then return end
	
    self.Owner:DrawWorldModel( false )
	
end	
	
	
/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
    
    if (!SERVER) then return end		
	 
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 
	
	if (tr.HitWorld) then return end
	
    if !tr.Entity:IsValid() then return end

	if tr.Entity:IsPlayer() then
	
		if ( tr.Entity.arrested ) then
		
			self.Owner:RedNotify( "This player is already handcuffed." )
			return
			
		elseif ( CurTime() < self.NextUse ) then

			self.Owner:RedNotify( "Please wait before using handcuffs on a player again." )
			return 
			
		end	
	
		tr.Entity:SetWalkSpeed( 100 )
		tr.Entity:SetRunSpeed( 100 )
		tr.Entity:StripWeapons()		
		tr.Entity.canRun = false
		tr.Entity.arrested = true
		tr.Entity.arrester = self.Owner
		tr.Entity:RedNotify( "You are handcuffed, you will be auto un-handcuffed in "..Configuration["handcufftime"].." seconds." )
		
		timer.Simple( Configuration["handcufftime"], function()
		
			if tr.Entity:IsValid() then
		
				tr.Entity:SetWalkSpeed( 150 )
				tr.Entity:SetRunSpeed( 300 )
				tr.Entity:LoadOut()
				tr.Entity.canRun = true
				tr.Entity.arrested = false			
				tr.Entity.arrester = nil			
				tr.Entity:GreenNotify( "You have been un-handcuffed." )
				
			end	

		end )
		
		self.NextUse = CurTime() + 5		
		
	end

end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()

end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end

