
// Variables that are used on both client and server

SWEP.Author		= "Slob187"
SWEP.Contact		= "slob187.pb@gmail.com"
SWEP.Purpose		= "Lock and unlock doors."
SWEP.Instructions	= "Left click to lock.\nRight click to unlock."

SWEP.ViewModel		= "models/Weapons/V_hands.mdl"
SWEP.WorldModel		= "models/weapons/w_crowbar.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextUse = 0


/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType("normal")	
						
end
	
/*---------------------------------------------------------
	Deploy
---------------------------------------------------------*/
function SWEP:Deploy()

    if (!SERVER) then return end
    self.Owner:DrawWorldModel( false )
	
end	
	
	
/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
    
	if CurTime() < self.NextUse then return end
	 
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 
	
	if (tr.HitWorld) then return end
	
    if (!SERVER) then return end		
	
    if !tr.Entity:IsValid() then return end

	if tr.Entity:isOwnable() then
	
		if tr.Entity:IsOwnedBy(self.Owner) then
	
			tr.Entity:Fire( "lock", "", 0 )
			tr.Entity:EmitSound( "doors/default_locked.wav" )
			
			self.NextUse = CurTime() + .3
			
		elseif tr.Entity.coOwners != nil then
		
			if table.HasValue( tr.Entity.coOwners, self.Owner:SteamID64() ) then
			
				tr.Entity:Fire( "lock", "", 0 )
				tr.Entity:EmitSound( "doors/default_locked.wav" )
				
				self.NextUse = CurTime() + .3

			end
			
		end	

	end
	
end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()
    	
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 		
		
	if CurTime() < self.NextUse then return end
	
    if (!SERVER) then return end		
	
    if !tr.Entity:IsValid() then return end
	
	if tr.Entity:isOwnable() then
	
		if tr.Entity:IsOwnedBy(self.Owner) then
	
			tr.Entity:Fire( "unlock", "", 0 )
			tr.Entity:EmitSound( "doors/door_latch3.wav" )
			
			self.NextUse = CurTime() + .3
			
		elseif tr.Entity.coOwners != nil then
		
			if table.HasValue( tr.Entity.coOwners, self.Owner:SteamID64() ) then
			
				tr.Entity:Fire( "unlock", "", 0 )
				tr.Entity:EmitSound( "doors/door_latch3.wav" )
				
				self.NextUse = CurTime() + .3	

			end
			
		end	

	end	

end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end

