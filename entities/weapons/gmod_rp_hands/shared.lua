
// Variables that are used on both client and server

SWEP.Author		= "Slob187"
SWEP.Contact		= "slob187.pb@gmail.com"
SWEP.Purpose		= "Hands."
SWEP.Instructions	= "Left click to punch.\nRight click to knock the door."

SWEP.ViewModel		= "models/Weapons/V_hands.mdl"
SWEP.WorldModel		= "models/weapons/w_camphone.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextPunch = 0
SWEP.NextKnock = 0


/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType("normal")	
						
end
	
/*---------------------------------------------------------
	Deploy
---------------------------------------------------------*/
function SWEP:Deploy()

    if (!SERVER) then return end
    self.Owner:DrawWorldModel(false)
	
end	
	
	
/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
    
	if CurTime() < self.NextPunch then return end
	 
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 
	
	if (tr.HitWorld) then return end
	
    if (!SERVER) then return end		
	
    if !tr.Entity:IsValid() then return end

	if !tr.Entity:IsPlayer() then 
		if tr.Entity:isOwnable() then
			DoKnock(tr.Entity)
			self.NextPunch = CurTime() + .15
		end 
		return 
	end
	
	tr.Entity:EmitSound("physics/flesh/flesh_impact_bullet"..math.random(1, 5)..".wav")
	tr.Entity:TakeDamage( 3, self.Owner , self.Weapon )
	self.NextPunch = CurTime() + 1.5
		
end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()
    	
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 		
		
	if CurTime() < self.NextKnock then return end
	
    if (!SERVER) then return end	
	
	if !tr.Entity:IsValid() then return end
	
	if !string.find( string.lower( tr.Entity:GetClass() ), "door" ) then return end
	DoKnock(tr.Entity)
	self.NextKnock = CurTime() + .15
	
end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end

function DoKnock(ent)
	ent:EmitSound("physics/wood/wood_crate_impact_hard"..math.random(2, 2)..".wav")
end

