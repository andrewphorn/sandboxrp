
// Variables that are used on both client and server

SWEP.Author		= "Slob187"
SWEP.Contact		= "slob187.pb@gmail.com"
SWEP.Purpose		= "Extinguish props."
SWEP.Instructions	= "Left click to use."

SWEP.ViewModel		= "models/weapons/v_RPG.mdl"
SWEP.WorldModel		= "models/weapons/w_rocket_launcher.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextUse = 0


/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType( "rpg" )	
						
end
	

/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
    
	if CurTime() < self.NextUse then return end
	 
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 1000
	trace.filter = self.Owner
	local tr = util.TraceLine(trace) 
	

	for k, v in pairs( ents.FindInSphere( tr.HitPos, 300 ) ) do
	
		if (SERVER) then
	
			if v:IsOnFire() then		
			
				v:Extinguish()	
				v:SetColor( Color(255, 255, 255, 255) )
				v:EmitSound( "ambient/fire/gascan_ignite1.wav" )
				
			end
			
		end
		
		self:ShootBullet( 0, 0, 0 )

	end
	
	self.NextUse = CurTime() + 3	

end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()

end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end


function SWEP:ShootBullet( damage, num_bullets, aimcone )
	
	local bullet = {}
	bullet.Num 		= num_bullets
	bullet.Src 		= self.Owner:GetShootPos()			// Source
	bullet.Dir 		= self.Owner:GetAimVector()			// Dir of bullet
	bullet.Spread 	= Vector( aimcone, aimcone, 0 )		// Aim Cone
	bullet.Tracer	= 1									// Show a tracer on every x bullets 
	bullet.Force	= 30								// Amount of force to give to phys objects
	bullet.Damage	= damage
	bullet.AmmoType = "Pistol"
	bullet.HullSize = 2
	bullet.TracerName = "water_stream"
	
	self.Owner:FireBullets( bullet )

end
