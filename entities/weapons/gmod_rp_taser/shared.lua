
// Variables that are used on both client and server

SWEP.Author		= "Slob187"
SWEP.Contact		= "slob187.pb@gmail.com"
SWEP.Purpose		= "Defend / attack."
SWEP.Instructions	= "Left click to use."

SWEP.ViewModel		= "models/weapons/v_Pistol.mdl"
SWEP.WorldModel		= "models/weapons/W_Alyx_Gun.mdl"

SWEP.Spawnable      = false
SWEP.AdminSpawnable = false

util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.NextTaser = 0

/*---------------------------------------------------------
	Initialize
---------------------------------------------------------*/
function SWEP:Initialize()

    if (!SERVER) then return end
	
    self:SetWeaponHoldType("pistol")	

end
	

/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
	
end


/*---------------------------------------------------------
	PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
    
	if CurTime() < self.NextTaser then return end
	 
	local trace = {}
    trace.start = self.Owner:EyePos()
	trace.endpos = trace.start + self.Owner:GetAimVector() * 85
	trace.filter = self.Owner
	local tr = util.TraceLine( trace ) 
	
	if ( tr.HitWorld ) then return end
	
    if !tr.Entity:IsValid() then return end

	if !tr.Entity:IsPlayer() then return end
	
 	local effectdata = EffectData() 
 		effectdata:SetOrigin( tr.HitPos ) 
 		effectdata:SetNormal( tr.HitNormal ) 
 		effectdata:SetMagnitude( 8 ) 
 		effectdata:SetScale( 1 ) 
 		effectdata:SetRadius( 10 ) 
 	util.Effect( "Sparks", effectdata ) 
	
	tr.Entity:EmitSound( "weapons/stunstick/spark"..math.random( 1, 3 )..".wav" )
	
	if (SERVER) then
	
		tr.Entity:TakeDamage( 5, self.Owner, self.Weapon )
		
	end	
	
	self.NextTaser = CurTime() + 1.5

end

/*---------------------------------------------------------
	SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()
	
end

/*---------------------------------------------------------
	Think does nothing 
---------------------------------------------------------*/
function SWEP:Think()

end

