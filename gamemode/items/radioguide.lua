local ITEM = {}


ITEM.ID = "radioguide"

ITEM.Name = "Radiosystem using guide"
ITEM.Price = 6
ITEM.Info = "A book that tells you how to use the radio system."
ITEM.Type = "book"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/binderblue.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseBook( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseBookUse( p, ent )
	
end


GMRP.AddItem( ITEM )