local ITEM = {}


ITEM.ID = "bigbeer"

ITEM.Name = "Big beer"
ITEM.Price = 20
ITEM.Info = "A big bottle of beer."
ITEM.Type = "alcohol"
ITEM.Remove = true
ITEM.Energy = 20
ITEM.Model = "models/props_junk/garbage_glassbottle003a.mdl"
ITEM.Permit = "alcoholselling"
ITEM.Merchant = true


function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseUse( p, ITEM )
	p:SetNWBool( "ishigh", true )
	
	timer.Simple( 15, function()
	
		if p:IsValid() then
		
			p:SetNWBool( "ishigh", false )
		
		end
		
	end )
	
end


GMRP.AddItem(ITEM)