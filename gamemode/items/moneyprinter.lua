local ITEM = {}

ITEM.ID = "moneyprinter"

ITEM.Name = "Money Printer"
ITEM.Price = 500
ITEM.Info = "Print money! Has a chance of exploding."
ITEM.Type = "blackmarket"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/props_c17/consolebox01a.mdl"
ITEM.Permit = "blackmarket"
ITEM.Merchant = false
ITEM.Enforce_Owner = true

function ITEM.Spawn( p )

	GMRP.BaseSent( p, "gm_rp_moneyprinter", ITEM )
	
end


function ITEM.Use( p, ent )
	

end


GMRP.AddItem( ITEM )