local ITEM = {}


ITEM.ID = "lemonade"

ITEM.Name = "A bottle of lemonade"
ITEM.Price = 15
ITEM.Info = "Fresh lemonade."
ITEM.Type = "drink"
ITEM.Remove = true
ITEM.Energy = 35
ITEM.Model = "models/props_junk/garbage_plasticbottle003a.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true

function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end

function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


