local ITEM = {}

ITEM.ID = "airboat"

ITEM.Name = "Airboat"
ITEM.Price = 300
ITEM.Info = "An airboat."
ITEM.Type = "vehicle"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/airboat.mdl"
ITEM.Permit = "vehicleselling"
ITEM.Merchant = true


function ITEM.Spawn( p )

	GMRP.BaseVehicle( p, "models/airboat.mdl", "prop_vehicle_airboat", "scripts/vehicles/airboat.txt", ITEM )

end


function ITEM.Use( p, ent )

end


GMRP.AddItem( ITEM )