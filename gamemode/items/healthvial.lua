local ITEM = {}


ITEM.ID = "healthvial"

ITEM.Name = "Healthvial"
ITEM.Price = 10
ITEM.Info = "A healthvial duh."
ITEM.Type = "health"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/healthvial.mdl"
ITEM.Permit = false
ITEM.Merchant = false

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	local HealthAmount = 30

	if ( p:Health() + HealthAmount ) >= 100 then 
		p:SetHealth( 100 ) 	
	else 
		p:SetHealth( p:Health() + HealthAmount )
	end
end


GMRP.AddItem(ITEM)


