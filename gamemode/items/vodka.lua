local ITEM = {}


ITEM.ID = "vodka"

ITEM.Name = "Russian vodka"
ITEM.Price = 50
ITEM.Info = "A bottle of Russian vodka."
ITEM.Type = "alcohol"
ITEM.Remove = true
ITEM.Energy = 30
ITEM.Model = "models/props_junk/glassjug01.mdl"
ITEM.Permit = "alcoholselling"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseUse( p, ITEM )
	p:SetNWBool( "ishigh", true )
	
	timer.Simple( 35, function()
	
		if p:IsValid() then
		
			p:SetNWBool( "ishigh", false )
		
		end
		
	end )
	
end


GMRP.AddItem(ITEM)