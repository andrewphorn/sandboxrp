local ITEM = {}


ITEM.ID = "orangeplant"

ITEM.Name = "An orange plant"
ITEM.Price = 10
ITEM.Info = "An orange plant."
ITEM.Type = "plant"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_foliage/tree_deciduous_03b.mdl"
ITEM.Permit = "farming"
ITEM.Merchant = false

function ITEM.Spawn( p )

	GMRP.BaseCreator( p, "orange", ITEM )
	
end

function ITEM.Use( p, ent )
	
end


GMRP.AddItem( ITEM )