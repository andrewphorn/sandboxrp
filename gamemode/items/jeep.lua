local ITEM = {}

ITEM.ID = "jeep"

ITEM.Name = "Jeep"
ITEM.Price = 600
ITEM.Info = "A jeep."
ITEM.Type = "vehicle"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/buggy.mdl"
ITEM.Permit = "vehicleselling"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseVehicle( p, "models/buggy.mdl", "prop_vehicle_jeep", "scripts/vehicles/jeep_test.txt", ITEM )
	
end


function ITEM.Use( p, ent )

end


GMRP.AddItem(ITEM)