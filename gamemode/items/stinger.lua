local ITEM = {}

ITEM.ID = "stinger"

ITEM.Name = "A stinger"
ITEM.Price = 30
ITEM.Info = "Place on the road to stop cars."
ITEM.Type = "special"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/props_wasteland/dockplank01b.mdl"
ITEM.Permit = "disabled"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseSent( p, "gm_rp_stinger", ITEM )
	
end


function ITEM.Use( p, ent )
	

end


GMRP.AddItem( ITEM )