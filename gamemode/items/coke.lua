local ITEM = {}


ITEM.ID = "coke"

ITEM.Name = "A bottle of coke"
ITEM.Price = 3
ITEM.Info = "Can of a cold coke."
ITEM.Type = "drink"
ITEM.Remove = true
ITEM.Energy = 18
ITEM.Model = "models/props_junk/PopCan01a.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true


function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end

function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


