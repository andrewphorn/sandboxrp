local ITEM = {}


ITEM.ID = "melonplant"

ITEM.Name = "A melon plant"
ITEM.Price = 30
ITEM.Info = "A melon plant."
ITEM.Type = "plant"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_foliage/tree_deciduous_03b.mdl"
ITEM.Permit = "farming"
ITEM.Merchant = false

function ITEM.Spawn( p )

	GMRP.BaseCreator( p, "melon", ITEM )
	
end

function ITEM.Use( p, ent )
	
end


GMRP.AddItem( ITEM )