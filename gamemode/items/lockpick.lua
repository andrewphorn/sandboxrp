local ITEM = {}

ITEM.ID = "lockpick"

ITEM.Name = "A professional lockpick"
ITEM.Price = 10
ITEM.Info = "A professional lockpick to unlock doors."
ITEM.Type = "blackmarket"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/weapons/w_crowbar.mdl"
ITEM.Merchant = true
ITEM.Permit = "blackmarket"


function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end


function ITEM.Use( p, ent )

	p:Give( "gmod_rp_lockpick" )	

end


GMRP.AddItem( ITEM )