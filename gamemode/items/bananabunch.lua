local ITEM = {}


ITEM.ID = "bananabunch"

ITEM.Name = "Banana bunch"
ITEM.Price = 10
ITEM.Info = "A bunch of bananas."
ITEM.Type = "food"
ITEM.Remove = true
ITEM.Energy = 100
ITEM.Model = "models/props/cs_italy/bananna_bunch.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true


function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end


function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


