local ITEM = {}


ITEM.ID = "bananaplant"

ITEM.Name = "A banana plant"
ITEM.Price = 25
ITEM.Info = "A banana plant."
ITEM.Type = "plant"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_foliage/tree_deciduous_03b.mdl"
ITEM.Permit = "farming"
ITEM.Merchant = false

function ITEM.Spawn( p )

	GMRP.BaseCreator( p, "bananabunch", ITEM )
	
end

function ITEM.Use( p, ent )
	
end


GMRP.AddItem( ITEM )