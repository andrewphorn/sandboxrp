local ITEM = {}


ITEM.ID = "milk"

ITEM.Name = "Milk"
ITEM.Price = 10
ITEM.Info = "Fresh milk."
ITEM.Type = "drink"
ITEM.Remove = true
ITEM.Energy = 50
ITEM.Model = "models/props_junk/garbage_milkcarton002a.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true

function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end

function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


