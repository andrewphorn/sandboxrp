local ITEM = {}


ITEM.ID = "healthkit"

ITEM.Name = "A health kit"
ITEM.Price = 15
ITEM.Info = "An effective healthkit."
ITEM.Type = "health"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/Items/HealthKit.mdl"
ITEM.Permit = "medicine"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	p:SetHealth(100)
	local HealthAmount = 100

	if ( p:Health() + HealthAmount ) >= 100 then 
		p:SetHealth( 100 ) 
		p:GreenNotify( "You ate enough." )		
	else 
		p:SetHealth( p:Health() + HealthAmount )
		p:GreenNotify("+"..HealthAmount.." health")		
	end
end


GMRP.AddItem(ITEM)


