local ITEM = {}


ITEM.ID = "themingebag"

ITEM.Name = "The MingeBag story by Biohazard"
ITEM.Price = 12
ITEM.Info = "A book about the MingeBag."
ITEM.Type = "book"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/binderblue.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseBook( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseBookUse( p, ent )
	
end


GMRP.AddItem( ITEM )