local ITEM = {}


ITEM.ID = "smallbeer"

ITEM.Name = "Small beer"
ITEM.Price = 15
ITEM.Info = "A small bottle of beer."
ITEM.Type = "alcohol"
ITEM.Remove = true
ITEM.Energy = 15
ITEM.Model = "models/props_junk/GlassBottle01a.mdl"
ITEM.Permit = "alcoholselling"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseUse( p, ITEM )
	p:SetNWBool( "ishigh", true )
	
	timer.Simple( 10, function()
	
		if p:IsValid() then
		
			p:SetNWBool( "ishigh", false )
		
		end
		
	end )
	
end


GMRP.AddItem(ITEM)