local ITEM = {}


ITEM.ID = "arandomday"

ITEM.Name = "A random day book"
ITEM.Price = 10
ITEM.Info = "A book about a random day."
ITEM.Type = "book"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/binderblue.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseBook( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseBookUse( p, ent )
	
end


GMRP.AddItem( ITEM )