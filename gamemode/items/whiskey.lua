local ITEM = {}


ITEM.ID = "whiskey"

ITEM.Name = "Whiskey"
ITEM.Price = 45
ITEM.Info = "A bottle of whiskey."
ITEM.Type = "alcohol"
ITEM.Remove = true
ITEM.Energy = 25
ITEM.Model = "models/props_junk/garbage_glassbottle002a.mdl"
ITEM.Permit = "alcoholselling"
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseUse( p, ITEM )
	p:SetNWBool( "ishigh", true )
	
	timer.Simple( 30, function()
	
		if p:IsValid() then
		
			p:SetNWBool( "ishigh", false )
		
		end
		
	end )
	
end


GMRP.AddItem(ITEM)