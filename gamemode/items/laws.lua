local ITEM = {}


ITEM.ID = "laws"

ITEM.Name = "Laws book"
ITEM.Price = 5
ITEM.Info = "A book that contains current laws."
ITEM.Type = "book"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/binderblue.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseBook( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseBookUse( p, ent )
	
end


GMRP.AddItem( ITEM )