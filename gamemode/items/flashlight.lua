local ITEM = {}


ITEM.ID = "flashlight"

ITEM.Name = "A flashlight"
ITEM.Price = 5
ITEM.Info = "A flashlight, duh."
ITEM.Type = "item"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/props_wasteland/light_spotlight02_lamp.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	

	local sitem = ents.Create( "prop_physics" )
	sitem:SetModel( ITEM.Model )
	sitem:SetPos( tr.HitPos )
	sitem:Spawn()
	sitem:Activate()
	sitem:GetPhysicsObject():Wake()	
	sitem:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
	sitem.ID = ITEM.ID
	sitem.IsItem = true
	sitem.Use = ITEM.Use
	sitem.Removeable = ITEM.Remove

end

function ITEM.Use( p, ent )

	p.hasFlashlight = true
	
end


GMRP.AddItem( ITEM )