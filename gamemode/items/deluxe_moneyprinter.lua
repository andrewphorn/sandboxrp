local ITEM = {}

ITEM.ID = "deluxemoneyprinter"

ITEM.Name = "Deluxe Money Printer"
ITEM.Price = 1800
ITEM.Info = "Straight from Money Printer Artisans in Japan!"
ITEM.Type = "blackmarket"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/props_c17/consolebox01a.mdl"
ITEM.Permit = "blackmarket"
ITEM.Merchant = false
ITEM.IColor = Color(130, 210, 0, 255)
ITEM.Enforce_Owner = true

function ITEM.Spawn( p )

	GMRP.BaseSent( p, "gm_rp_moneyprinter_deluxe", ITEM )
	
end


function ITEM.Use( p, ent )
	
end


GMRP.AddItem( ITEM )