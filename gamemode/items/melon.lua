local ITEM = {}


ITEM.ID = "melon"

ITEM.Name = "Water melon"
ITEM.Price = 13
ITEM.Info = "A water melon."
ITEM.Type = "food"
ITEM.Remove = true
ITEM.Energy = 100
ITEM.Model = "models/props_junk/watermelon01.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true


function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end


function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


