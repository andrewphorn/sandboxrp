local ITEM = {}


ITEM.ID = "drugplant"

ITEM.Name = "Drug plant"
ITEM.Price = 1000
ITEM.Info = "Use to get drugs."
ITEM.Type = "blackmarket"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/cactus.mdl"
ITEM.Merchant = true
ITEM.Permit = "blackmarket"

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )
	
	local cEntIndex = ent:EntIndex()
	
	if ( p.plant_hasDrugs == nil ) then
	
		p.plant_hasDrugs = {}
		
	end
	
	if ( p.plant_hasDrugs[cEntIndex] == nil ) then
	
		p.plant_hasDrugs[cEntIndex] = 0
		
	end	

	if ( p.plant_hasDrugs[cEntIndex] > CurTime() ) then
	
		p:RedNotify( "This plant does not have drugs yet, please try again in "..math.Round( p.plant_hasDrugs[cEntIndex] - CurTime() ).." second(s)." )	
	
	else
	
		local drugs = GMRP.Items["drugs"]
	
		local sitem = ents.Create( "prop_physics" )
		sitem:SetModel( drugs.Model )
		sitem:SetPos( ent:GetPos() + Vector( 0, 0, 50 ) )
		sitem:Spawn()
		sitem:Activate()
		sitem:GetPhysicsObject():Wake()	
		sitem:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
		sitem.ID = "drugs"
		sitem.IsItem = true
		sitem.Use = drugs.Use

		sitem.Removeable = true
		p.plant_hasDrugs[cEntIndex] = CurTime() + 60	
		
	end

end


GMRP.AddItem(ITEM)


