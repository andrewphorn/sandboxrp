local ITEM = {}


ITEM.ID = "noodles"

ITEM.Name = "Chienese noodles."
ITEM.Price = 10
ITEM.Info = "Fresh chienese noodles."
ITEM.Type = "food"
ITEM.Remove = true
ITEM.Energy = 40
ITEM.Model = "models/props_junk/garbage_takeoutcarton001a.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true

function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end

function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


