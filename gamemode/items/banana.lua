local ITEM = {}


ITEM.ID = "banana"

ITEM.Name = "A banana"
ITEM.Price = 3
ITEM.Info = "A banana."
ITEM.Type = "food"
ITEM.Remove = true
ITEM.Energy = 30
ITEM.Model = "models/props/cs_italy/bananna.mdl"
ITEM.Permit = false
ITEM.Merchant = false


function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end


function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


