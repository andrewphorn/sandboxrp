local ITEM = {}


ITEM.ID = "oxygen_tank"

ITEM.Name = "An air tank"
ITEM.Price = 30
ITEM.Info = "An air tank to swim under the water."
ITEM.Type = "item"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/props_c17/canister01a.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )
		
	p.O2 = true 
	p:GreenNotify("O2 tank charged.") 
		
end


GMRP.AddItem(ITEM)


