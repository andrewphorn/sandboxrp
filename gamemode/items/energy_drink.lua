local ITEM = {}


ITEM.ID = "energy_drink"

ITEM.Name = "Mega energy drink"
ITEM.Price = 15
ITEM.Info = "A bottle of a mega energy drink."
ITEM.Type = "drink"
ITEM.Remove = true
ITEM.Energy = 100
ITEM.Model = "models/props_junk/garbage_plasticbottle001a.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true

function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end

function ITEM.Use( p, ent )
		
	GAMEMODE:SetPlayerSpeed( p, 500, 500 )
	
	timer.Simple( 30, function()
		GAMEMODE:SetPlayerSpeed( p, 150, 300 )
	end)	
	
	GMRP.BaseUse( p, ITEM )
	
end


GMRP.AddItem(ITEM)


