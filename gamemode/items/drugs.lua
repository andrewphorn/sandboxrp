local ITEM = {}


ITEM.ID = "drugs"

ITEM.Name = "Drug jar"
ITEM.Price = 250
ITEM.Info = "Use to get high."
ITEM.Type = "blackmarket"
ITEM.Remove = true
ITEM.Energy = 100
ITEM.Model = "models/props_lab/jar01a.mdl"
ITEM.Merchant = true
ITEM.Permit = "blackmarket"

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )
	
	if p:Health() > 10 then
	
		p:SetNWBool( "ishigh", true )
		p:SetHealth( p:Health() - 10 )		
		p:SetJumpPower( 300 )
		
		GAMEMODE:SetPlayerSpeed( p, 500, 500 )
		
		timer.Simple( 30, function()
		
			if p:IsValid() then
		
				p:SetNWBool( "ishigh", false )
				p:SetJumpPower(160)
				GAMEMODE:SetPlayerSpeed( p, 150, 300 )

			end
				
		end )	
		
	else
	
		p:Kill()
		
	end	
	
end


GMRP.AddItem(ITEM)


