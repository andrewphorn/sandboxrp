local ITEM = {}

ITEM.ID = "taser"

ITEM.Name = "A taser"
ITEM.Price = 300
ITEM.Info = "Use it to defend or attack."
ITEM.Type = "item"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/weapons/W_pistol.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end


function ITEM.Use( p, ent )
		
	p:Give( "gmod_rp_taser" )	

end


GMRP.AddItem( ITEM )