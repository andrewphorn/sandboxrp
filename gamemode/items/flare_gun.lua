local ITEM = {}


ITEM.ID = "flare_gun"

ITEM.Name = "A flare gun"
ITEM.Price = 250
ITEM.Info = "A flare gun to call for help."
ITEM.Type = "item"
ITEM.Remove = true
ITEM.Energy = 0
ITEM.Model = "models/weapons/W_pistol.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

	p:Give("gmod_rp_flare")	

end


GMRP.AddItem(ITEM)