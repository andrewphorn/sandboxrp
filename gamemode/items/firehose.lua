local ITEM = {}

ITEM.ID = "firehose"

ITEM.Name = "A firehose"
ITEM.Price = 35
ITEM.Info = "Use on fires."
ITEM.Type = "special"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/weapons/w_rocket_launcher.mdl"
ITEM.Permit = "firefighter"
ITEM.UsePermit = "firefighter"
ITEM.Merchant = false


function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end


function ITEM.Use( p, ent )
		
	GMRP.BaseSpecialUse( p, ITEM, "gmod_rp_firehose", ent )	

end


GMRP.AddItem( ITEM )