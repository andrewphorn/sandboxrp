local ITEM = {}


ITEM.ID = "radiostation"

ITEM.Name = "Professional radiostation"
ITEM.Price = 150
ITEM.Info = "Simple rope an antenna to your radiostation for mega far tranmition range."
ITEM.Type = "item"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/props_lab/servers.mdl"
ITEM.Permit = false
ITEM.Merchant = true


function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	p:SendLua([[Derma_Message("Radiostation itself has really small transmition range, so you need to buy an antenna and simple rope it to the radiostation to get the huge transmition range.", "Instructions", "Ok")]])

end


function ITEM.Use( p, ent )

	if ent.ID == "radiostation" then
	
		if ent.ChangedChanel == nil then 
		
			p:SendLua([[Derma_StringRequest("Radiostation", "Enter the radiostation's name.", "Enter your radiochanel's name here.", function(text) RunConsoleCommand("gm_rp_radiostation_create", text) end)]]) 	
			
		else	
		
			p:SendLua([[GMRP.RadioStationMenu()]])	
			
		end
		
	end	

end


GMRP.AddItem( ITEM )