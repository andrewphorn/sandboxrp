local ITEM = {}


ITEM.ID = "orange"

ITEM.Name = "An orange"
ITEM.Price = 1
ITEM.Info = "An orange."
ITEM.Type = "food"
ITEM.Remove = true
ITEM.Energy = 10
ITEM.Model = "models/props/cs_italy/orange.mdl"
ITEM.Permit = "foodselling"
ITEM.Merchant = true

function ITEM.Spawn( p )
	GMRP.BaseItemSpawn( p, ITEM )
end


function ITEM.Use( p, ent )
	GMRP.BaseUse( p, ITEM )
end


GMRP.AddItem(ITEM)


