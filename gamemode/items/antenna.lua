local ITEM = {}


ITEM.ID = "antenna"

ITEM.Name = "An antenna"
ITEM.Price = 50
ITEM.Info = "An antenna for professional radiostation."
ITEM.Type = "item"
ITEM.Remove = false
ITEM.Energy = 0
ITEM.Model = "models/props_rooftop/antenna01a.mdl"
ITEM.Merchant = true
ITEM.Permit = false

function ITEM.Spawn( p )

	GMRP.BaseItemSpawn( p, ITEM )
	
end

function ITEM.Use( p, ent )

end


GMRP.AddItem(ITEM)


