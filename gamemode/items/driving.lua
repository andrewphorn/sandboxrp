local ITEM = {}


ITEM.ID = "driving"

ITEM.Name = "A vehicle driving guide"
ITEM.Price = 8
ITEM.Info = "A book that tells you how to drive."
ITEM.Type = "book"
ITEM.Remove = false
ITEM.Energy = 100
ITEM.Model = "models/props_lab/binderblue.mdl"
ITEM.Permit = false
ITEM.Merchant = true

function ITEM.Spawn( p )

	GMRP.BaseBook( p, ITEM )
	
end

function ITEM.Use( p, ent )

	GMRP.BaseBookUse( p, ent )
	
end


GMRP.AddItem( ITEM )