--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


if file.Exists( "sandboxrp/settings/rpsettings2.txt" ,"DATA") then

	Configuration = util.KeyValuesToTable( file.Read( "sandboxrp/settings/rpsettings2.txt" ) )
	
else
	
	Configuration = {} --define the configuration table
	Configuration["knockouttax"] = 100 --knock out tax
	Configuration["icrange"] = 600 --in character talk range
	Configuration["talkrange"] = 500 --mic talk range
	Configuration["doorcost"] = 50 --door cost
	Configuration["paydayint"] = 180 --pay day interval ( in seconds )
	Configuration["jobchangeint"] = 120 --job change interval (in seconds)
	Configuration["startmoney"] = 200 --start money
	Configuration["useint"] = 1 --use interval
	Configuration["maxdoors"] = 7 --max doors owned per person
	Configuration["propcost"] = 10 --prop price
	Configuration["handcufftime"] = 60 --time of player being handcuffed (in seconds)	
	Configuration["allbuild"] = 0 --all players can build without paying money for props - 1 to enable, 0 to disable
	Configuration["allbuildandpay"] = 0 --all players can build, but they pay money for props - 1 to enable, 0 to disable
	Configuration["vehiclespawning"] = 0 --vehicle spawning - 1 to enable, 0 to disable
	Configuration["ragdollspawning"] = 0 --ragdoll spawning - 1 to enable, 0 to disable
	Configuration["effectspawning"] = 0 --effect spawning - 1 to enable, 0 to disable
	Configuration["swepspawning"] = 0 --SWEP spawning - 1 to enable, 0 to disable	
	Configuration["sentspawning"] = 0 --SENT spawning - 1 to enable, 0 to disable
	Configuration["alltool"] = 0 --no tool restrictions - 1 to enable, 0 to disable
	Configuration["realistic"] = 1 --realistic sandbox settings (no noclip, damage enabled) - 1 to enable, 0 to disable
	Configuration["limitedphysgun"] = 1 --limited physgun (range, weight) - 1 to enable, 0 to disable
	Configuration["economy"] = 1 --economy - 1 to enable, 0 to disable
	Configuration["economyinterval"] = 180 --economy interval (in seconds)
	Configuration["doortextfadedist"] = 500 -- how far away from a door you can be before the text is 100% transparent
	
	file.Write( "sandboxrp/settings/rpsettings2.txt", util.TableToKeyValues( Configuration ) )
	
end
