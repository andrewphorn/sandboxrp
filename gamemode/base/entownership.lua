local playerMeta = FindMetaTable( "Player" )

function playerMeta:ownEnt()

	local trace = {}
	trace.start = self:EyePos()
	trace.endpos = trace.start + self:GetAimVector() * 85
	trace.filter = self
	local tr = util.TraceLine( trace )	
	
	local ent = tr.Entity
	
	local playerNick = self:Nick()	
	
	if !ent:IsValid() then return end
	
	if ent:isOwnable() and !ent:GetNW2Bool("ent_unownable") then
	
		if ent:GetEntOwner() == false then
	
			if self:HasMoney( Configuration[ "doorcost" ] ) then
		
				if self.alreadyOwned == nil then		
		
					self.alreadyOwned = 0
					
				end

				if ( self.alreadyOwned < Configuration["maxdoors"] ) then
		
					ent:SetEntOwner(self)
					self:TakeMoney( Configuration[ "doorcost" ] )
					
					self.alreadyOwned = self.alreadyOwned + 1
					
				else

					self:RedNotify( "You own too much doors, please unown some and try again." )
				
				end
			
			end	
			
		else

			if ent:IsOwnedBy(self) then
			
				self:ConCommand( "gm_rp_door_menu" )
				
			end	
			
		end	

	end

end


function GMRP.addCoOwner( p, c, a )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine( trace )	
	
	local ent = tr.Entity
	
	local playerNick = p:Nick()	
	
	local strName = string.lower( string.Implode( " ", a ) )
	
	if !ent:IsValid() then return end
	
	if ent:IsOwnedBy(p) then
	
		for k, v in pairs( player.GetAll() ) do
		
			if string.find( string.lower( v:Nick() ), strName ) then
				
				if v:IsValid() then
				
					if ent.coOwners == nil then 
					
						ent.coOwners = {}
						
					end

					local coOwner = {name=v:Nick(), sid=v:SteamID64()}
				
					table.insert( ent.coOwners, coOwner )
				
				end
			
			end
		
		end	

	end
	
end	

GMRP.command( "door_addowner", GMRP.addCoOwner )


function playerMeta:unOwnEnts()

	for k, v in pairs( ents.GetAll() ) do
	
		if v:isOwnable() then
		
			if v:IsOwnedBy(self) then
			
				v:SetEntOwner()
				self.alreadyOwned = self.alreadyOwned - 1			
			
			end
			
		end
		
	end
	
end


function GMRP.sellEnt( p )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine( trace )	
	
	local ent = tr.Entity
	
	local playerNick = p:Nick()	
	
	if !ent:IsValid() then return end

	if ent:IsOwnedBy(p) then

		ent:SetEntOwner()
		p.alreadyOwned = p.alreadyOwned - 1			
		p:AddMoney( Configuration[ "doorcost" ] )
		p:GreenNotify( "Door sold!" )
		
	end	
	
end	

GMRP.command( "door_sell", GMRP.sellEnt )


function GMRP.removeCoOwner( p, c, a )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine( trace )	
	
	local ent = tr.Entity
	
	local playerNick = p:Nick()	
	
	local strName = string.lower( string.Implode( " ", a ) )
	
	if !ent:IsValid() then return end
	
	if ent.coOwners == nil then return end
	
	if !table.HasValue( ent.coOwners, strName ) then return end
	
	if ent:IsOwnedBy(p) then
	
		for k, v in pairs( player.GetAll() ) do
		
			if string.find( string.lower( v:Nick() ), strName ) then
		 
				if v:IsValid() then 

					local coOwnerNick = v:Nick()
					
					for k, v in pairs( ent.coOwners ) do
					
						if v == coOwnerNick then
						
							ent.coOwners[k] = nil
							
						end

					end
					
				end	
				
			end
			
		end
		
	end	
	
end

GMRP.command( "door_removeowner", GMRP.removeCoOwner )	


function playerMeta:cleanEntOwnerShip()

	for k, v in pairs( ents.GetAll() ) do
	
		if v:isOwnable() then
		
			if v:IsOwnedBy(self) then
			
				v:SetEntOwner()
				self.alreadyOwned = self.alreadyOwned - 1					
				
			end	
			
			if v.coOwners != nil then
			
				for k, v in pairs( v.coOwners ) do
				
					if v == self:Nick() then
					
						v.coOwners[k] = nil
						
					end

				end

			end

		end

	end

end	


function GM:ShowSpare2( p )

	p:ownEnt()

end