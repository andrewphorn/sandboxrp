function GMRP.BuyItem( ply, command, args )

	for itemname, item in pairs(GMRP.Items) do
	
		if tostring( args[1] ) == itemname then
		
			if GMRP.UseLimit( ply ) then

				local permit_exists = false

				if !ply:CanBuyItem(item) then
					ply:RedNotify("You can't buy that item. Nice try!")
					return
				end
				
				if ply:HasMoney( tonumber( item.Price ) ) then 
				
					ply:TakeMoney( item.Price )
					ply:GreenNotify( item.Name.." has been purchased and put in your inventory." )
					GMRP.AddToInentory( ply, itemname )
					
				else
				
					ply:RedNotify("You do not have enough money to purchase that!")
					
				end
				
			end	
		end
	end
	
end	

GMRP.command( "buyitem", GMRP.BuyItem )
