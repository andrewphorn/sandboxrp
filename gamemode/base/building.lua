--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


if file.Exists( "sandboxrp/settings/blockedprops.txt", "DATA" ) then

	mingeprops = util.KeyValuesToTable( file.Read( "sandboxrp/settings/blockedprops.txt" ) )
	
else
	
	file.Write( "sandboxrp/settings/blockedprops.txt", util.TableToKeyValues( { "crane_frame", "huge", "bridge", "building", "chair", "explosive" } ) )
	
end


if file.Exists( "sandboxrp/settings/blockedtools.txt", "DATA" ) then

	mingetools = util.KeyValuesToTable( file.Read( "sandboxrp/settings/blockedtools.txt" ) )
	
else

	file.Write( "sandboxrp/settings/blockedtools.txt", util.TableToKeyValues( { "paint", "trails", "dynamite", "turret", "duplicator", "ignite", "nocollide" } ) )
	
end


local emeta = FindMetaTable( "Entity" )

function emeta:ghostOn()

	local e = self:GetColor()
	local opacity = 180
	self:SetColor( Color(e.r, e.g, e.b, opacity ) )
	self:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
	
end


function emeta:ghostOff()

	local e = self:GetColor()
	self:SetColor( Color(e.r, e.g, e.b, 255 ) )
	self:SetCollisionGroup( COLLISION_GROUP_NONE )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
		
end

function GM:CanTool( p, tr, toolmode )
	
	local class = tr.Entity:GetClass()
	
	if !self.BaseClass:CanTool( p, tr, toolmode ) || tr.Entity:IsPlayer() then
	
		return false
		
	elseif string.find( class, "door" ) then
	
		return false
		
	elseif table.HasValue( mingetools, toolmode ) then
	
		p:RedNotify( toolmode.." tool is disabled" )
		return false		
		
	elseif tr.Entity.IsItem || string.find( class, "vehicle" ) then
	
		if toolmode == "remover" then
	
			return false
			
		end

	elseif ( !p:HasPermit("building") ) && ( Configuration["alltool"] == 0 ) then

		if toolmode != "rope" then
		
			p:RedNotify( "Need to have building permit to use tools other than rope." )
			
			return false
			
		else
		
			if ( tr.HitWorld ) then
			
				p:RedNotify( "You can not use the rope tool on this." )
				return false
				
			end	
			
		end

	end	
	
	return true

end


function GM:PlayerSpawnVehicle( p )

	if Configuration["vehiclespawning"] == 0 then

		p:RedNotify( "Vehicle spawning is disabled." )
	
		return false
		
	end	

	return true
	
end	


function GM:PlayerSpawnRagdoll( p, model )

	if Configuration["ragdollspawning"] == 0 then

		p:RedNotify( "Ragdoll spawning is disabled." )
	
		return false
		
	end	

	return true
	
end	


function GM:PlayerSpawnEffect( p, model )

	if Configuration["effectspawning"] == 0 then

		p:RedNotify( "Effect spawning is disabled." )
	
		return false
		
	end	

	return true
	
end	


function GM:PlayerSpawnSWEP( p, classname )

	if Configuration["swepspawning"] == 0 then

		p:RedNotify( "SWEP spawning is disabled." )
	
		return false
		
	end	

	return true
	
end
	

function GM:PlayerSpawnSENT( p, sent_type ) 

	if Configuration["sentspawning"] == 0 then

		p:RedNotify( "SENT spawning is disabled." )
	
		return false
		
	end	

	return true
	
end	


function GM:PlayerSpawnProp( ply, mdl )

	if !self.BaseClass:PlayerSpawnProp( ply, mdl ) then
	
		return false
		
	end	
		
	for k, v in pairs( mingeprops ) do
	
		if string.find( string.lower( mdl ), v ) then
		
			ply:RedNotify( "This prop is blocked." )
			
			return false
			
		end
		
	end				
		
	if Configuration["allbuild"] == 1 then
	
		return true
		
	elseif Configuration["allbuildandpay"] == 1 then

		if !ply:HasMoney( Configuration["propcost"] ) then
			
			return false
			
		end
	
		ply:TakeMoney( Configuration["propcost"] )
		ply:RedNotify( "Paid "..CUR..Configuration["propcost"].." to spawn a prop." )	
		
		return true
		
	elseif ( !ply:HasPermit( "building" ) ) then
	
		ply:RedNotify( "Need to have building permit active to spawn props." )
		
		return false
		
	elseif !ply:HasMoney( Configuration["propcost"] ) then
	
		return false
		
	end
	
	ply:TakeMoney( Configuration["propcost"] )
	ply:RedNotify( "Paid "..CUR..Configuration["propcost"].." to spawn a prop." )
	
	return true

end