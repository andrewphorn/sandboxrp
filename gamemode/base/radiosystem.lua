Radio = {}

function Radio.RadioAllowed( ent )

	if !ent:IsValid() then return false end
	
	if ent.ID == "radio" then
	
		return true
		
	end
	
	return false
	
end	


function Radio.RadioStationAllowed( ent )

	if !ent:IsValid() then return false end
	
	if ent.ID != "radiostation" then return false end
	
	return true
	
end	


function Radio.PlaySound( ent, sound )

	ent.TheSound = CreateSound( ent, sound )
	ent.TheSound:Play()
	
end	
	

function Radio.StopSound( ent )

	if ent.TheSound != nil then
	
		ent.TheSound:Stop()
		ent.TheSound = nil
		
	end
	
end		


function Radio.CPStopSound( p )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	
	
	if !Radio.RadioStationAllowed( tr.Entity ) then return end
	
	for k, v in pairs(ents.GetAll()) do
	
		if tr.Entity.RadioStationName == v.RadioStation then
	
			if v.TheSound != nil then
			
				Radio.StopSound( v )
				
			end
			
		end	
		
	end

end

GMRP.command ( "radiostation_stop", Radio.CPStopSound )	


function Radio.ChooseType( name, p )

	if !p:isFavPermit( "radiobroadcasting" ) then
	
		return tostring( name ).."_pirate"
		
	end	
	
	return tostring( name )

end	


function Radio.CreateRadiostation( p, c ,a )


	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)

	local ImplArgs = tostring( string.Implode( " ", a ) )
	
	if tostring( ImplArgs ) == nil then return end
	
	if string.find( ImplArgs, "'" ) then
	
		ImplArgs = string.gsub( ImplArgs, "'", "" )
		
	end	
	
	if table.HasValue(	Radiostations, tostring( ImpArgs ) ) then
	
		p:RedNotify("This radiostation already exists.")
		
		return 
		
	end
	
	if !tr.Entity.ChangedChanel then
	
		if Radio.RadioStationAllowed( tr.Entity ) then
	
			table.insert( Radiostations, Radio.ChooseType( tostring( ImplArgs ), p ) )	
			RunLuaAll( "table.insert( Radiostations, '"..Radio.ChooseType( tostring( ImplArgs ), p ).."' )")
			tr.Entity.RadioStationName = Radio.ChooseType( tostring( ImplArgs ), p )
			tr.Entity.ChangedChanel = true	
		
		end	
		
	end
	
end

GMRP.command ( "radiostation_create", Radio.CreateRadiostation )	
	

function Radio.ChangeChanel( p, c, a )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)
	
	local ImplArgs = tostring( string.Implode( " ", a ) )	
	
	if tostring( ImplArgs ) == nil then return end	

	if !tr.Entity:IsValid() then return end
	
	if tr.Entity.ID == "radio" then
	
		if table.HasValue( Radiostations, tostring( ImplArgs ) ) then
		
			Radio.StopSound( tr.Entity )
		
			tr.Entity.RadioStation = tostring( ImplArgs )
			tr.Entity:EmitSound( "ambient/levels/prison/radio_random"..math.random( 2, 9 )..".wav" )
			tr.Entity:SetNWString( "radiostation", tostring( ImplArgs ) )
			
		end	
		
	end	

end	

GMRP.command( "radio_changechanel", Radio.ChangeChanel )
	

function Radio.HasAntenna( ent )

	local Constrained = constraint.GetAllConstrainedEntities( ent )
	
	if ent.IDs != nil then
	
		ent.IDs = nil
		
	end
	
	ent.IDs = {}
	
	for _, v in pairs( Constrained ) do
	
		if v.ID != nil then
		
			table.insert( ent.IDs, v.ID ) --insert the ent.ID's
			
		end
		
	end

	if table.HasValue( ent.IDs, "antenna" ) then
	
		return true
		
	end
	
	return false

end


function Radio.TransmitSound( p, c, a )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	
	
	local ImplArgs = tostring( string.Implode( " ", a ) )	
	
	if !Radio.RadioStationAllowed( tr.Entity, p ) then return end
	
	tr.Entity.Range = 3000
	
	if Radio.HasAntenna(tr.Entity) then
	
		tr.Entity.Range = 800000
		
	else
	
		tr.Entity.Range = 10000
		
	end	
	
	for k, v in pairs( ents.FindInSphere( tr.Entity:GetPos(), tr.Entity.Range ) ) do
	
		if Radio.RadioAllowed( v ) && tr.Entity.RadioStationName == v.RadioStation then
		
			if v.TheSound != nil then
			
				Radio.StopSound( v )
				Radio.PlaySound( v, tostring( ImplArgs ) )
				v.CurrentSound = tostring( ImplArgs )
				
			else
			
				Radio.PlaySound( v, tostring( ImplArgs ) )
				v.CurrentSound = tostring( ImplArgs )				
				
			end	
			
		end
		
	end

end	

GMRP.command( "radiostation_sound", Radio.TransmitSound)


function GMRP.DeleteRadiostation( ent )


	if ent.RadioStationName != nil then
	
		for k, v in pairs( Radiostations ) do
		
			if v == ent.RadioStationName then
			
				table.remove( Radiostations, k )				
				RunLuaAll( "table.remove( Radiostations, "..k..")" )
				
			end
			
		end
		
	end	
				
end