ChatCommands = {}

function GMRP.ChatCommand( name, cmd )
	
	ChatCommands[name] = cmd
	
end	


function GM:PlayerSay( p, text )

	local hookedchat = hook.Call("PlayerSay", nil, p, text)

	if hookedchat == "" then
		return ""
	end

	if hookedchat ~= nil then
		text = tostring(hookedchat)
	end
	
	local chat = string.Explode( " ", string.lower(text) )
	
	for k, v in pairs( ChatCommands ) do
	
		if chat[1] == k then
		
			if chat[2] != "" then
		
				v( p, chat[2] )
				return ""
				
			else

				v( p )
				return ""
				
			end	
			
		end
		
	end	
	

	if string.sub( text, 1, 2 ) == "//" then
	
		return string.sub( text, 3 )
		
	else
		-- we're doing an rptalk
		local actionText = false
		if string.sub(text, 1, 4) == "/me " then
			actionText = true
		end
		p:RPTalk( Configuration["icrange"], text, actionText )	
		return ""
		
	end	

	
	return text

end	


function DropMoneyChatCommand(ply, amount)

	if args == "" then return "" end
	
	ply:ConCommand("gm_rp_dropcash "..amount)
	
end

GMRP.ChatCommand( "/drop", DropMoneyChatCommand )
	
	