local pmeta = FindMetaTable("Player")

function pmeta:CheckHunger()

	if !self:Alive() then return end
	
	if self.NextHungerThink == nil then
	
		self.NextHungerThink = CurTime() + 10
		
	end	
	
	if CurTime() > self.NextHungerThink then
	
		self:TakeEnergy( 1 )
		
		self.NextHungerThink = CurTime() + 10

	end
	
end	


function pmeta:HungerDeath()
	
	self:TakeDamage( 20, self, self )
	self:RealisticDamage()
	self:RedNotify( "You are starving, go and eat some food." )
	
end	


function pmeta:AddEnergy(amount)

	local am = tonumber( amount )

	if ( self:GetPlayerEnergy() + am ) >= 100 then 
	
		self:SetPlayerEnergy( 100 ) 	
		
	else 
	
		self:SetPlayerEnergy( self:GetPlayerEnergy() + am )
		self:GreenNotify("+"..am.." energy")		
		
	end

end


function pmeta:TakeEnergy( amount )

	if self:GetNWInt( "energy" ) - amount > 0 then
	
		self:SetNWInt( "energy", self:GetNWInt("energy") - amount )
		
	else
	
		self:SetNWInt( "energy", 0 )
		self:HungerDeath()
		
	end
	
end	


function pmeta:AddHealth( amount )

	local am = tonumber( amount )

	if ( self:Health() + am ) >= 100 then 
	
		self:SetHealth( 100 ) 	
		
	else 
	
		self:SetHealth( self:Health() + am )
		self:GreenNotify("+"..am.." health")		
		
	end

end		