local pmeta = FindMetaTable("Player")
local MoneyName = "tehmoneylol"


function pmeta:SetupAccount()

	if self:GetPData( MoneyName ) == nil then
	
		self:SetPData( MoneyName, tonumber( Configuration["startmoney"] ) )
		
	end
	
end
	

function pmeta:GetMoney()

	return tonumber(self:GetPData(MoneyName))
	
end


function pmeta:SetMoney(amount)

	self:SetPData(MoneyName, tonumber(amount))
	self:SetNWInt(MoneyName, tonumber(amount))
	
end


function pmeta:AddMoney(amount)

	self:SetPData(MoneyName, tonumber(self:GetPData(MoneyName)) + tonumber(amount))
	self:SetNWInt(MoneyName, tonumber(self:GetPData(MoneyName)))	
	
end


function pmeta:HasMoney(amount)

	if tonumber(self:GetPData(MoneyName)) >= tonumber(amount) then
	
		return true
		
	else
	
		self:RedNotify( "You do not have enough money." )
		return false
		
	end	
	
end	


function pmeta:TakeMoney(amount)

	if self:HasMoney(tonumber(amount)) then
	
		self:SetPData(MoneyName, tonumber(self:GetPData(MoneyName)) - tonumber(amount))
		self:SetNWInt(MoneyName, tonumber(self:GetPData(MoneyName)))	
		
	end		
	
end


function pmeta:CorrectMoney(amount)

	self:SetNWInt(MoneyName, tonumber(self:GetPData(MoneyName)))
	
end	


function GMRP.DropMoney( ply, command, args )

	local MoneyAmount = math.floor( tonumber( args[1] ) )
	
	if MoneyAmount == nil then return end
	
	if GMRP.UseLimit( ply ) then
	
		if MoneyAmount <= 0 then
		
			ply:RedNotify("Invalid amount !")
			
		elseif ply:HasMoney(MoneyAmount) then	
		
			local trace = {}
			trace.start = ply:EyePos()
			trace.endpos = trace.start + ply:GetAimVector() * 85
			trace.filter = ply
			local tr = util.TraceLine(trace)
		
			ply:TakeMoney( MoneyAmount ) 
			ply:GreenNotify( CUR..MoneyAmount.." droped." )
			
			local money = ents.Create( "prop_physics" )
			money:SetModel( "models/weapons/w_suitcase_passenger.mdl" )
			money:SetPos( tr.HitPos )
			money:Spawn()
			money:Activate()
			money:GetPhysicsObject():Wake()	
			money:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
			money.Money = true
			money.Amount = MoneyAmount	
			money:SetNW2Bool("isMoney", true)
			money:SetNW2Int("moneyAmount", MoneyAmount)
			
		end
	
	end
	
end

GMRP.command( "dropcash", GMRP.DropMoney)