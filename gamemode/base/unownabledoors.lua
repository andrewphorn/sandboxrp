--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]
UNOWNABLE_DOORS = UNOWNABLE_DOORS or {}

function GM:InitPostEntity()

	timer.Simple( 5, function()
	
		print( "GModRP: Loading unownable doors..." )

		local rDoorsF = "sandboxrp/doors/"
		local curMap = game.GetMap()
		local unOwnableDoors = {}


		if !file.Exists( rDoorsF.."rp_oviscity_gmc4.txt", "DATA" ) then

			local ovisDoors = { "-2624.8301 -1080.0000 -331.0000", "-2490.0000 -2068.0000 -330.0000",
			"-1767.0472 -1910.9688 -327.2162", "-468.0313 -1535.8856 -337.0611", "-459.4050 -535.0000 -330.0000",
			"-6696.0000 191.5000 -808.0000", "-420.1800 2420.7000 -330.2810", "-1140.0900 2363.2800 -330.2810",
			"-4474.0000 1152.0000 -328.0000", "-6632.0396 594.0180 -808.0000", "-6632.0396 427.9820 -808.0000" }
			
			file.Write( rDoorsF.."rp_oviscity_gmc4.txt", util.TableToKeyValues( ovisDoors ) )
			
		end


		GMRP.LoadUnownableDoors()
		
		print( "GModRP: Unownable doors loaded." )
		
	end )	
	
	self.BaseClass:InitPostEntity()	
	
end

GMRP.SetUnownable = function(door)
	if door:isOwnable() then
		door:SetNW2Bool("ent_unownable", true)
		door:SetEntOwner()
		table.insert(UNOWNABLE_DOORS, door:GetPos())
		print("Added an unownable door...")
	end
end

GMRP.SetOwnable = function(door)
	local door_unownable = door:GetNW2Bool("ent_unownable")
	if !door_unownable then
		return
	end
	if door:isOwnable() then
		door:SetNW2Bool("ent_unownable", false)
		door:SetEntOwner()
	end
end

GMRP.LoadUnownableDoors = function()
	local fpath = "sandboxrp/doors/"
	local fmap = game.GetMap()
	local dfile = fpath..fmap..'.txt'
	local door_data = {}
	UNOWNABLE_DOORS = {}

	if (file.Exists(dfile, "DATA")) then
		door_data = util.KeyValuesToTable( file.Read( dfile ) )
		for _, ent in pairs( ents.GetAll() ) do
			if (ent:isOwnable()) then 
				for _, vec in pairs(door_data) do
					if tostring(ent:GetPos()) == vec then
						GMRP.SetUnownable(ent)
					end
				end
			end
		end
	end
end

GMRP.SaveUnownableDoors = function()
	local fpath = "sandboxrp/doors/"
	local fmap = game.GetMap()
	local door_data = {}

	for k, vec in pairs(UNOWNABLE_DOORS) do
		table.insert(door_data, tostring(vec))
	end

	file.Write(fpath..fmap..".txt", util.TableToKeyValues(door_data))
end

GMRP.GetDoorLookingAt = function( p ) 
	local pTrace = p:GetEyeTrace()
	local traceEnt = pTrace.Entity

	if traceEnt:isOwnable() then

		return traceEnt

	end

	local doors_nearby = ents.FindInSphere(pTrace.HitPos, 20)

	for k,v in pairs(doors_nearby) do
		if v:isOwnable() then
			return v
		end
	end

	return false
end

GMRP.command( "door_getpos", function( p )

	if GMRP.UseLimit( p ) then
	
		local traceEnt = GMRP.GetDoorLookingAt( p )

		if traceEnt then

			p:PrintMessage( HUD_PRINTCONSOLE, tostring( traceEnt:GetPos() ) )

		else

			p:ChatPrint( "Trace entity is not door / vehicle!" )
			
		end	
		
	end
	
end )	

GMRP.command( "door_unownable", function( p )
	if (!p:IsAdmin()) then return end
	local door = GMRP.GetDoorLookingAt( p )
	if door then
		GMRP.SetUnownable(door)
		GMRP.SaveUnownableDoors()
	end
end )

GMRP.command( "door_ownable", function( p )
	if (!p:IsAdmin()) then return end
	local door = GMRP.GetDoorLookingAt( p )
	if door then
		GMRP.SetOwnable(door)
		GMRP.SaveUnownableDoors()
	end
end )

GMRP.command( "door_own_debug", function( p )
	p:PrintMessage( HUD_PRINTCONSOLE, tostring( #UNOWNABLE_DOORS ) )
	GMRP.LoadUnownableDoors()
	p:PrintMessage( HUD_PRINTCONSOLE, tostring( #UNOWNABLE_DOORS ) )
	local door = GMRP.GetDoorLookingAt( p )
	if door then
		p:PrintMessage( HUD_PRINTCONSOLE, tostring(door:GetEntOwner()))
	end
end)