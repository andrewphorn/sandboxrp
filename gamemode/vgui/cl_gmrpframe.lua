--[[

	By Slob187.
	DFrame by Garry Newman.
	
  ]]	

 function DrawCircle( x, y, radius, seg )
	local cir = {}

	table.insert( cir, { x = x, y = y, u = 0.5, v = 0.5 } )
	for i = 0, seg do
		local a = math.rad( ( i / seg ) * -360 )
		table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )
	end

	local a = math.rad( 0 ) -- This is needed for non absolute segment counts
	table.insert( cir, { x = x + math.sin( a ) * radius, y = y + math.cos( a ) * radius, u = math.sin( a ) / 2 + 0.5, v = math.cos( a ) / 2 + 0.5 } )

	surface.DrawPoly( cir )
end

function DrawRotatedRect( x, y, w, h, rot, x0, y0 )

	local c = math.cos( math.rad( rot ) )
	local s = math.sin( math.rad( rot ) )

	local newx = y0 * s - x0 * c
	local newy = y0 * c + x0 * s

	surface.DrawTexturedRectRotated( x + newx, y + newy, w, h, rot )

end
	
PANEL = {}

AccessorFunc( PANEL, "m_bDraggable", 		"Draggable", 		FORCE_BOOL )
AccessorFunc( PANEL, "m_bSizable", 			"Sizable", 			FORCE_BOOL )
AccessorFunc( PANEL, "m_bScreenLock", 		"ScreenLock", 		FORCE_BOOL )
AccessorFunc( PANEL, "m_bDeleteOnClose", 	"DeleteOnClose", 	FORCE_BOOL )
AccessorFunc( PANEL, "m_bBackgroundBlur", 	"BackgroundBlur", 	FORCE_BOOL )

bgblur = Material("pp/blurscreen")

GMRP.OpenFrames = {}

/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:Init()

	self:SetFocusTopLevel( true )
	
	self.ButtonPos = 115	
	self.Sidebar = 0
	self.BGOpacity = 255
	
	self:SetDraggable( true )
	self:SetSizable( true )
	self:SetScreenLock( true )
	self:SetDeleteOnClose( true )
	
	-- This turns off the engine drawing
	self:SetPaintBackgroundEnabled( true )
	self:SetPaintBorderEnabled( true )
	
	-- Set size of panel
	self:SetSize( 700, 700 )

	-- Set creation time
	self.m_fCreateTime = SysTime()

	-- initialise the title bar
	self:InitTitleBar()

end

function PANEL:ResizeTitleBar()
	local CenteredExitBtn = (self.TitleBarH / 2) - (self.btnClose:GetTall() / 2)

	self.btnClose:SetPos( self.btnClose:GetParent():GetWide() - self.btnClose:GetWide() - CenteredExitBtn, CenteredExitBtn )
	self.lblTitle:SetPos( CenteredExitBtn + 5, CenteredExitBtn )
	self.lblTitle:SetSize( self:GetWide() - self.btnClose:GetWide() - CenteredExitBtn - CenteredExitBtn, self.TitleBarH )

end

function PANEL:InitTitleBar()
	-- define titlebar size
	self.TitleBarH = 30

	self.TitleBar = vgui.Create( "DPanel", self )
	self.TitleBar:SetTall(self.TitleBarH)
	self.TitleBar:Dock(TOP)
	self.TitleBar:DockPadding(7,2,2,2)
	self.TitleBar:SetDragParent(self)
	function self.TitleBar:OnMousePressed() self:GetParent():OnMousePressed() end
	function self.TitleBar:OnMouseReleased() self:GetParent():OnMouseReleased() end
	function self.TitleBar:Paint(w,h)
		draw.NoTexture()
		surface.SetDrawColor(0,0,0,60)
		surface.DrawRect(0,0,w,h)
	end

	-- Add close button
	self.btnClose = vgui.Create( "DButton", self.TitleBar )
	self.btnClose:SetVisible( false )
	self.btnClose:Dock(RIGHT)
	self.btnClose:SetText( "" ) -- no text, we draw an x manually
	self.btnClose.DoClick = function ( button ) self:Close() end
	self.btnClose.Paint = function( panel, w, h ) 

		-- draw the squircle
		draw.NoTexture()
		surface.SetDrawColor(221,74,74,255)
		DrawCircle(w/2, h/2, w/2, 30)

		-- set up for the x, color
		surface.SetDrawColor(140, 10, 10)

		-- set up x part sizes
		local rectW = 5
		local rectH = h * .6

		-- draw the two lines
		DrawRotatedRect(w/2, h/2, rectW, rectH, 45, 0, 0)
		DrawRotatedRect(w/2, h/2, rectW, rectH, 135, 0, 0)
	end
	-- Set close btn size
	self.btnClose:SetSize( 24, 24 )

	-- Add Title label
	self.lblTitle = vgui.Create( "DLabel", self.TitleBar )
	self.lblTitle:Dock(FILL)
	self.lblTitle:SetFont("GMRPTITLE")
	self.lblTitle:SetColor(Color(76, 186, 245))
	self.lblTitle:SetVisible(true)

end

function PANEL:SidebarWidth()
	return self.Sidebar * 150
end

/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:ShowCloseButton( bShow )
	if bShow == true then
		for i,panel in pairs(GMRP.OpenFrames) do
			if isfunction(panel.Close) then
				panel:Close()
			end
			GMRP.OpenFrames[i] = nil
		end
		table.insert(GMRP.OpenFrames, self)
	end
	self.btnClose:SetVisible( bShow )
end

/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:SetTitle( strTitle )

	self.lblTitle:SetText( strTitle )

end


function PANEL:DrawList()

	self.List = vgui.Create( "DPanelList", self )
	self.List:Dock(FILL)
	self.List:DockMargin(5,5,5,5)
	self.List:EnableVerticalScrollbar( true ) 
	
end


function PANEL:List_SetAutoSize( bool )

	if self.List != nil then
	
		self.List:SetAutoSize( bool )
		
	end	
	
end	


function PANEL:List_AddItem( item )

	if self.List != nil then
	
		self.List:AddItem( item )
		
	end	
	
end	


function PANEL:List_EnableHorizontal( bool )

	if self.List != nil then
	
		if tobool( bool ) == nil then

			self.List:EnableHorizontal( false )
			
			return
			
		end	
		
		self.List:EnableHorizontal( bool )
		
	end	
	
end


function PANEL:List_Spacing( number )

	if self.List != nil then
	
		if tonumber( number ) == nil then
		
			self.List:SetSpacing( 5 )
			
			return
			
		end	
		
		self.List:SetSpacing( number )
		
	end	
	
end


function PANEL:List_Padding( number )

	if self.List != nil then
	
		if tonumber( number ) == nil then
		
			self.List:SetPadding( 15 )
			
			return
			
		end	
		
		self.List:SetPadding( number )
		
	end	
	
end

function PANEL:AddButton( name, cmd )
	self.Sidebar = 1

	self.AddedButton = vgui.Create( "DButton", self )
	self.AddedButton:SetSize( 120, 20 )
	self.AddedButton:SetPos( 10, self.ButtonPos )
	self.AddedButton:SetText( name )
	self.AddedButton.DoClick = function()
	
		LocalPlayer():ConCommand( cmd )
		
	end
	
	self.ButtonPos = self.ButtonPos + 25
	
end	


function PANEL:AddFuncButton( name, func )
	self.Sidebar = 1

	self.AddedButton = vgui.Create( "DButton", self )
	self.AddedButton:SetSize( 120, 20 )
	self.AddedButton:SetPos( 10, self.ButtonPos )
	self.AddedButton:SetText( name )
	self.AddedButton.DoClick = function()
	
		func()
		
	end
	
	self.ButtonPos = self.ButtonPos + 25
	
end	
	
	
/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:Close()

	self:SetVisible( false )

	if ( self:GetDeleteOnClose() ) then
		self:Remove()
	end

end


/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:Center()

	self:InvalidateLayout( true )
	self:SetPos( ScrW()/2 - self:GetWide()/2, ScrH()/2 - self:GetTall()/2 )

end


/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:Paint(w, h)
	local x,y = self:GetPos()
	local opacity = self.BGOpacity
	local opacity_mult = opacity / 255
	render.SetScissorRect(x,y,x+self:GetWide(),y+self:GetTall(), true)

	if ( self.m_bBackgroundBlur ) then
		if opacity * .9 > 1 then
			surface.SetDrawColor(255,255,255,opacity * .9)

			-- create a stronger blur
			local blurIter = 3
			local blurStrength = 5
			for i= 1,blurIter do
				bgblur:SetFloat("$blur", (i / blurIter) * blurStrength )
				bgblur:Recompute()
				render.UpdateScreenEffectTexture()
				surface.SetMaterial(bgblur)
				surface.DrawTexturedRect(x  * -1, y * -1, ScrW(), ScrH())
			end
		end
		draw.NoTexture() 
	end

	render.SetScissorRect(x,y,x+self:GetWide(),y+self:GetTall(), false)

	--end
	surface.SetDrawColor(0,0,0,math.max(30, 120 * opacity_mult))
	surface.DrawRect(0, 0, self:GetWide(), self:GetTall())

	surface.SetDrawColor(0,0,0,opacity)
	surface.DrawOutlinedRect(0,0,self:GetWide(),self:GetTall())

	return true

end

function PANEL:OnMousePressed()

	local screenX, screenY = self:LocalToScreen( 0, 0 )

	if ( self:GetDraggable() && gui.MouseY() < ( screenY + 24 ) ) then
		self.Dragging = { gui.MouseX() - self.x, gui.MouseY() - self.y }
		self:MouseCapture( true )
		return
	end

end

function PANEL:OnMouseReleased()

	self.Dragging = nil
	self:MouseCapture( false )

end

function PANEL:Think()

	local mousex = math.Clamp( gui.MouseX(), 1, ScrW() - 1 )
	local mousey = math.Clamp( gui.MouseY(), 1, ScrH() - 1 )

	if ( self.Dragging ) then

		local x = mousex - self.Dragging[1]
		local y = mousey - self.Dragging[2]

		x = math.Clamp( x, 0, ScrW() - self:GetWide() )
		y = math.Clamp( y, 0, ScrH() - self:GetTall() )

		self:SetPos( x, y )

	end
end
/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:PerformLayout()

	derma.SkinHook( "Layout", "Frame", self )
end


/*---------------------------------------------------------

---------------------------------------------------------*/
function PANEL:IsActive()

	if ( self:HasFocus() ) then return true end
	if ( vgui.FocusedHasParent( self ) ) then return true end
	
	return false

end


derma.DefineControl( "GMRPFrame", "GMRP Frame", PANEL, "EditablePanel" )

