--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


GM.Name 		= "SandboxRP"
GM.Author 		= "Slob187"
GM.Email 		= "slob187pb@yahoo.com"
GM.Website 		= "https://gitlab.com/andrewphorn/sandboxrp"

--Sandbox Derived
DeriveGamemode( "sandbox" )

GMRP = GMRP or {}

CUR = "$"

--citizen models table
citizenmodels = {"models/player/group01/male_02.mdl",
				 "models/player/gman_high.mdl",
				 "models/player/odessa.mdl",
				 "models/player/Kleiner.mdl",
				 "models/player/monk.mdl",
				 "models/player/alyx.mdl",
				 "models/player/eli.mdl",
				 "models/player/Group01/male_07.mdl",
				 "models/player/breen.mdl",
				 "models/player/Group01/Female_01.mdl",
				 "models/player/Group01/male_06.mdl",
				 "models/player/mossman.mdl"
				}


Permits = {}
Permits["drive"] = { "Driver's License", 800 }
Permits["building"] = { "Building Permit", 450 }
Permits["farming"] = { "Farming", 300 }
Permits["radiobroadcasting"] = { "FCC Broadcasting License", 1000 }
Permits["foodselling"] = { "Merchant: Food Handler's License", 700 }
Permits["medicine"] = { "Merchant: Medicine License", 1100 }
Permits["alcoholselling"] = { "Merchant: Liquor License", 800 }
Permits["vehicleselling"] = { "Merchant: Vehicle Sale", 1300 }
Permits["blackmarket"] = { "Merchant: Black Market Contact", 2100 }
Permits["firefighter"] = { "EMS: Firefighter Training", 350 }
Permits["healer"] = { "EMS: Healing Training", 600 }
Permits["police_battering"] = { "Officer: Battering Ram License", 550 }
Permits["police_stunstick"] = { "Officer: Stunstick License", 1800 }
Permits["police_keys"] = {"Officer: Keys to the City", 3000}

GMRP.Permits = Permits

function GMRP.GetEntMass( ent )
	if CLIENT then
		local pobj = ent:GetPhysicsObject()
		if !IsValid(pobj) then
			return 10000
		end
		return pobj:GetMass()
	end

	local constr = constraint.GetAllConstrainedEntities( ent )	
	
	for k, v in pairs( constr ) do
	
		local phobj = v:GetPhysicsObject()
	
		if !phobj:IsValid() then return 0 end
		
		return phobj:GetMass()
		
	end	
	
end	

local pmeta = FindMetaTable("Player")

function pmeta:IsOutside()

	local trace = {}
	trace.start = self:GetShootPos()
	trace.endpos = trace.start + ( self:GetUp() * 350 )
	trace.filter = self
	local tr = util.TraceLine( trace )

	if !tr.HitWorld && !tr.HitNonWorld then
	
		return true
		
	end
	
	return false

end				

function pmeta:CrosshairMsg(str,color,silkicon)
	color = color or Color(255,255,255)
	str = tostring(str)
	silkicon = silkicon or ""
	if (SERVER) then
		net.Start("SBRPPacket")
		net.WriteString("CROSSHAIR_MSG")
		net.WriteString(str)
		net.WriteColor(color)
		net.WriteString(silkicon)
		net.Send(self)
	else
		CrosshairMsg(str, color, silkicon)
	end
end

function pmeta:CrosshairMsgErr(str)
	self:CrosshairMsg(str, Color(220,60,60), "exclamation")
end

function pmeta:Notify(str, color)
	color = color or Color(255,255,255)
	str = tostring(str)
	if (SERVER) then
		net.Start("SBRPPacket")
		net.WriteString("NOTIFY")
		net.WriteString(str)
		net.WriteColor(color)
		net.Send(self)
	else
		Notify(str, color)
	end
end

function pmeta:RedNotify(str)

	self:Notify(str, Color(255,60,60))
	
end


function pmeta:GreenNotify(str)

	self:Notify(str, Color(60,255,60))
	
end	


function pmeta:BlueNotify(str)

	self:Notify(str, Color(60,60,255))
	
end		


function pmeta:GetPlayerEnergy()

	return tonumber( self:GetNWInt( "energy" ) )
	
end	


include("itembase.lua")
if (SERVER) then

	AddCSLuaFile("itembase.lua")
	
end	


for k, v in pairs( file.Find( "sandboxrp/gamemode/items/*.lua","LUA") ) do

	include("items/"..v)
	
	if (SERVER) then
	
		AddCSLuaFile("items/"..v)
		
	end
	
end	

include("jobbase.lua")

if (SERVER) then

	AddCSLuaFile('jobbase.lua')

end

for k,v in pairs( file.Find( "sandboxrp/gamemode/jobs/*.lua", "LUA" ) ) do

	include("jobs/"..v)

	if (SERVER) then

		AddCSLuaFile("jobs/"..v)

	end

end

function GMRP.command( command, func )

	concommand.Add( "gm_rp_"..command, func)
	
end	
	

function GMRP.UseLimit( p )

	if CurTime() > p:GetNWInt( "NextUse" ) then 
		
		if (SERVER) then
		
			p:SetNWInt( "NextUse", CurTime() + Configuration["useint"] )
			
		end
		
		return true
		
	else

		p:RedNotify( "Please wait "..math.Round( p:GetNWInt( "NextUse" ) - CurTime() ).." second." )
		
		return false
		
	end

end


local emeta = FindMetaTable( "Entity" )

function emeta:isOwnable()

	local entClass = self:GetClass()

	if (string.find( entClass, "door" ) || string.find( entClass, "vehicle" )) && !self:IsWeapon() then

		return true
	
	end
	
	return false
	
end

function emeta:GetEntOwner()
	local sid64 = self:GetNW2String("entowner_sid")
	if sid64 then
		return player.GetBySteamID64(sid64)
	else
		return false
	end
end

function emeta:SetEntOwner(ply)
	if SERVER then
		local sid64 = 0
		if IsValid(ply) and ply:IsPlayer() then
			sid64 = ply:SteamID64()
		end
		self:SetNW2String("entowner_sid", sid64)
	end
end

function emeta:IsOwnedBy(ply)
	local owner = self:GetEntOwner()
	return owner and owner:SteamID64() == ply:SteamID64()
end

function pmeta:HasPermit( name )

	if (SERVER) then
	
		local PLoc = GMRP.GetPermitLocation( self )		

		if file.Exists( PLoc, "DATA" ) then
		
			local myPermits = util.KeyValuesToTable( file.Read( PLoc ) )
			
			if ( myPermits[name] != nil ) then
			
				return true
				
			end
			
		end
		
		return false
		
	else
	
		if ( MyPermits[name] != nil ) then
		
			return true
			
		end
		
		return false
		
	end	
	
end


function pmeta:GetFavPermit()

	local fPermit = self:GetNWString( "favpermit" )

	if ( fPermit != "" ) then
	
		return fPermit
		
	end
	
end


function pmeta:isFavPermit( name )

	if ( self:GetFavPermit() == name ) then
	
		return true
		
	end
	
	return false
	
end

function pmeta:CanBuyItem( item )
	return ((item.Merchant && self:GetJob().IsMerchant) || !item.Merchant) && ((item.Permit && self:HasPermit(item.Permit)) || !item.Permit)
end

function pmeta:GetJob()
	if (SERVER) then
		if (self.Job) then
			return self.Job
		else
			return GMRP.Jobs['default']
		end
	elseif (CLIENT) then
		local jobid = self:GetNWString("gm_rp_jobid")
		if (!jobid) then jobid = "default" end
		if (!GMRP.JobExists(jobid)) then jobid = "default" end
		if (self.Job && self.Job.ID ~= jobid) || (!self.Job) then
			self.Job = GMRP.Jobs[jobid]
		end
		return self.Job
	end
end

function pmeta:GetJobColor()
	local job = self:GetJob()
	return job.JobColor
end

function GM:OnContextMenuOpen()
    LocalPlayer():ConCommand( "gm_rp_playermenu" )
    return false
end

function GM:PhysgunPickup( ply, ent )

	if !self.BaseClass:PhysgunPickup( ply, ent ) then
		return false
	elseif ent:isOwnable() || ent:GetClass() == "func_breakable_surf" || ent:GetClass() == "func_breakable" then
		return false
	end 	
	
	if ply:GetNW2Bool("phys_limited") == true then
	
		if ply:GetPos():Distance( ent:GetPos() ) > 200 then
			if SERVER then
				ply:CrosshairMsgErr( "That is too far away" )
			end
			return false
		elseif GMRP.GetEntMass( ent ) > 300 then
			if SERVER then
				ply:CrosshairMsgErr( "That is too heavy" )
			end
			return false
		end	
	end	
	
	if CLIENT then 
		return true 
	end

	ent:ghostOn()
	return true
end


function GM:PhysgunDrop( p, ent )
	if CLIENT then
		return
	end
	ent:ghostOff()

	local physobj = ent:GetPhysicsObject()
	physobj:SetVelocity(Vector(0,0,0))
	physobj:Sleep()
	physobj:Wake()
end	

function GMRP.FormatMoney(amount)
  local formatted = amount
  while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return '$'..formatted
end