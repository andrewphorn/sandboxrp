--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


--Clientside includes
include('shared.lua')

local RenderScaleMult = 1 / .1

--fonts
surface.CreateFont("GMRPTITLE", {font = "coolvetica", size=26, weight=500, antialias=true})
surface.CreateFont("GModRPFontOne", {font = "coolvetica", size=24, weight=150, shadow=true, antialias=true})
surface.CreateFont("GModRPDoorFont", {font = "coolvetica", size=(5 * RenderScaleMult), weight=100, shadow=true, antialias=true})
surface.CreateFont("GModRPUITitleFont", {font = "coolvetica", size=30, weight=700, antialias=true})
surface.CreateFont("GModRPUIContentFont", {font = "coolvetica", size=20, weight=150, antialias=true})
surface.CreateFont("GModRPUIChatFont", {font = "Arial", size=17, weight=500, antialias=true, shadow=true})
surface.CreateFont("GModRPContentFont", {font = "coolvetica", size=24, weight=150, antialias=true})
surface.CreateFont("NotifyFontOne", {font = "Arial", size=35, weight=550, antialias=true, additive=false})

for k, v in pairs(file.Find("sandboxrp/gamemode/vgui/*.lua", "LUA")) do

	include("vgui/"..v)
	
end


for k, v in pairs(file.Find("sandboxrp/gamemode/derma/*.lua", "LUA")) do

	include("derma/"..v)
	
end

Radiostations = Radiostations or {}

local door_max_dist = 700
local global_doors = global_doors or {}
local player_pos = Vector(0,0,0)

function DrawShadowedText(txt,f,x,y,c,align_horiz,align_vert)
	align_horiz = align_horiz or TEXT_ALIGN_CENTER
	align_vert = align_vert or TEXT_ALIGN_TOP
	return draw.SimpleText(txt, f, x, y, c, align_horiz, align_vert)
end

function GM:HUDPaint()

	self.BaseClass:HUDPaint()

	local person = LocalPlayer()
	
	if ( !person:Alive() || person:GetNWBool( "sleeping" ) ) then return end
	
	local trace = {}
	trace.start = person:EyePos()
	trace.endpos = trace.start + person:GetAimVector() * 85
	trace.filter = person
	local tr = util.TraceLine( trace )		
	
	local ent = tr.Entity
	
	if !ent:IsValid() then return end
	
	if ent:GetNWString( "radiostation" ) != "" then
	
		draw.WordBox( 8, ScrW() / 2, ScrH() / 2, ent:GetNWString( "radiostation" ), "ChatFont", Color(50,50,75,100), Color(255,255,255,255) )	
	
	elseif ent:GetNWString( "permit" ) != "" then

		local owner = ent:GetEntOwner()
		if owner then owner = owner:Nick() else owner = "" end
		draw.WordBox( 8, ScrW() / 2, ScrH() / 2, owner.."'s "..ent:GetNWString( "permit" ).." permit", "ChatFont", Color(50,50,75,100), Color(255,255,255,255) )	
	
	elseif ent:GetNW2Bool( "isMoney" ) ~= false then
		DrawShadowedText("Money ("..GMRP.FormatMoney(ent:GetNW2Int("moneyAmount"))..") - E to pick up", "GModRPUITitleFont", ScrW() / 2 + 1, ScrH() / 2 - 34, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
	elseif ent:GetNW2String("ent_itemid") then
		local itemid = ent:GetNW2String("ent_itemid")
		local owner = ent:GetEntOwner()
		if GMRP.Items[itemid] then
			local item = GMRP.Items[itemid]
			if owner then owner=owner:Nick().."'s" else owner="Unowned" end
			DrawShadowedText(owner.." "..item.Name, "GModRPUITitleFont", ScrW() / 2 + 1, ScrH() / 2 - 34, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
		end
	end

	local unownable =  ent:GetNW2Bool("ent_unownable")
	if (!unownable) then
		if ent:isOwnable() && !person:InVehicle() then
			local hover_text = "Unowned Door - Press F4 to rent door!"
			local door_owner = ent:GetEntOwner()
			if door_owner ~= false then
				local door_owner = door_owner:Nick()
				hover_text = door_owner.."'s door"
			end
			DrawShadowedText(hover_text, "GModRPUITitleFont", ScrW() / 2 + 1, ScrH() / 2 - 34, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)

				
		end
	end

		
end


function GM:HUDShouldDraw( name )

	for k, v in pairs( {"CHudHealth", "CHudBattery", "CHudAmmo", "CHudSecondaryAmmo"} ) do
	
		if name == v then
		
			return false
			
		end
		
	end	
	
	return true
	
end

function CrosshairMsg(msg, c, icon)
	local countdown = 3
	local msgOpacity = 255
	if icon then
		icon = "icon16/"..icon..".png"
	end
	hook.Add("HUDPaint", "GModRPCHMSG", function()
		if (msgOpacity > .1) then
			c.a = msgOpacity
			local w,h = draw.SimpleTextOutlined(tostring(msg), "GModRPUITitleFont", ScrW() / 2, ScrH() *.55, c, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, Color(0,0,0,msgOpacity / 2))
			if icon then
				local iX = (ScrW() / 2) - (w / 2) - 20
				local iY = (ScrH() *.55) + ((h / 2) - 10)
				surface.SetDrawColor(255,255,255,msgOpacity)
				surface.SetMaterial(Material(icon))
				surface.DrawTexturedRect(iX, iY, 16, 16)
			end
		end
	end)
	timer.Simple(.1, function()
		timer.Create("RemoveCHMSG", 0.05, 0, function() 
			msgOpacity = msgOpacity - (255 * .05)
			if msgOpacity < 1 then
				timer.Destroy("RemoveCHMSG")
			end
		end)

	end)
end

function Notify( str, color )

	local na = 255
	
	hook.Add( "HUDPaint", "GModRPNotify", function()
		draw.DrawText( tostring( str ), "NotifyFontOne", 10, 35, Color( color.r, color.g, color.b, na ), 0 )	
		
	end )
	
	timer.Simple( 1.5, function()
	
		timer.Create( "RemoveNotify", 0.1, 0, function()
		
			na = na - 5
			
			if na <= 0 then
			
				timer.Destroy( "RemoveNotify" )
				hook.Remove( "HUDPaint", "GModRPNotify" )
				
			end
			
		end )
		
	end )
	
end


function GM:CalcView( ply, origin, angles, fov ) 

	if ply:Alive() then

		if ( ply:Health() <= 30 ) then
		
			angles.roll = angles.roll + ( math.sin( RealTime() ) * 3 )
			
		end	
			
		if ply:GetNWBool( "sitting" ) then
		
			origin = origin + Vector( 0, 0, -10 )
			angles = angles
			
		end	
		
	end
	
	return self.BaseClass:CalcView( ply, origin, angles, fov )
	
end

function GM:Move(ply)
	player_pos = ply:GetPos()
end


function GM:RenderScreenspaceEffects()
	
	self.BaseClass:RenderScreenspaceEffects()
	
	local person = LocalPlayer()
	local tab = {}
	
	if !person:Alive() then return end
	
	if ( person:Health() <= 30 ) then
	
		DrawMotionBlur( 0.3, 1, 0.05 )
		
	elseif ( person:GetNWInt( "energy" ) <= 15 ) then

		tab[ "$pp_colour_addr" ] = 0
		tab[ "$pp_colour_addg" ] = 0
		tab[ "$pp_colour_addb" ] = 0
		tab[ "$pp_colour_brightness" ] = ( math.sin( RealTime() ) / 10 )
		tab[ "$pp_colour_contrast" ] = 1
		tab[ "$pp_colour_colour" ] = 1
		tab[ "$pp_colour_mulr" ] = 0
		tab[ "$pp_colour_mulg" ] = 0
		tab[ "$pp_colour_mulb" ] = 0 
		
		DrawColorModify( tab )		
	
	elseif ( person:GetNWInt( "stamina" ) <= 10 ) then
	
		tab[ "$pp_colour_addr" ] = 0
		tab[ "$pp_colour_addg" ] = 0
		tab[ "$pp_colour_addb" ] = 0
		tab[ "$pp_colour_brightness" ] = .3
		tab[ "$pp_colour_contrast" ] = 1
		tab[ "$pp_colour_colour" ] = 1
		tab[ "$pp_colour_mulr" ] = 0
		tab[ "$pp_colour_mulg" ] = 0
		tab[ "$pp_colour_mulb" ] = 0 
		
		DrawColorModify( tab )		
	
	end	
	
	if person:GetNWBool( "sleeping" ) && person:Alive() then
	
		tab[ "$pp_colour_addr" ] = 0
		tab[ "$pp_colour_addg" ] = 0
		tab[ "$pp_colour_addb" ] = 0
		tab[ "$pp_colour_brightness" ] = -10
		tab[ "$pp_colour_contrast" ] = 1
		tab[ "$pp_colour_colour" ] = 1
		tab[ "$pp_colour_mulr" ] = 0
		tab[ "$pp_colour_mulg" ] = 0
		tab[ "$pp_colour_mulb" ] = 0 
		
		DrawColorModify( tab )
		
	elseif person:GetNWBool( "ishigh" ) then
	
		DrawSharpen( 1, 3 )

	end

end


function GM:OnPlayerChat( player, text, teamchat, alive )
	local player_nick = "CONSOLE"
	if isfunction(player.Nick) then
		player_nick = player:Nick()
	end
	chat.AddText( Color( 255,255,255,255 ), "[OOC] ", Color( 90, 111, 142, 225 ), player_nick, Color( 255,255,255,255 ), ": "..text )
	chat.PlaySound()
	
	return true
	
end

function GMRP.DrawDoors()
	for k,v in pairs(global_doors) do
		if v:isOwnable() then
			GMRP.DrawDoor(v)
		end
	end
end

function GMRP.DrawDoor(door)

	-- Figure out door size and initial orientation
	local ply = LocalPlayer()
	local plypos = player_pos
	local minB, maxB = door:GetModelBounds()
	local base_dangle = door:GetAngles()
	local door_width_x = math.abs(minB[1] - maxB[1])
	local door_width_y = math.abs(minB[2] - maxB[2])
	local door_width = door_width_y
	local base_pos = door:GetPos()
	if (math.Distance(base_pos.x, base_pos.y, plypos.x, plypos.y) > 650) then
		return true
	end
	local door_thickness = 2
	local door_owner = false

	-- figure out door opacity

	local distance_from_door = math.Distance(plypos.x, plypos.y, base_pos.x, base_pos.y)

	local door_text_opacity = math.Clamp((255 - ((distance_from_door / (door_max_dist * .60)) * 255)) + (door_max_dist * .1), 0, 255)

	-- can't see the door, don't bother rendering it...
	if (door_text_opacity == 0) then return end

	-- set text colors, feat. ya boi opacity
	local doorcolor_bad = Color(230,80,80,door_text_opacity)
	local doorcolor_good = Color(80, 230, 80,door_text_opacity)
	local doorcolor_info = Color(230, 230, 230,door_text_opacity)

	if (door_width_x > door_width_y) then
		base_dangle:RotateAroundAxis(base_dangle:Up(), -90)
		door_width = door_width_x
		door_thickness = door_width_y
		if (minB[1] * -1 > maxB[1]) then
			base_dangle.y = base_dangle.y + 180
		end
	else
		door_thickness = door_width_x
		if (minB[2] * -1 > maxB[2]) then
			base_dangle.y = base_dangle.y + 180
		end
	end

	if door:GetClass() == "func_door_rotating" then
		door_thickness = door_thickness * 4
	end


	local dangle = base_dangle

	local drawDoorSide = function(side)
		-- set up rotation
		local dpos = base_pos

		-- figure out door width and origin rotation

		door_width = door_width
		if (side == "front") then
			dangle:RotateAroundAxis(dangle:Right(), -90)
			dangle:RotateAroundAxis(dangle:Up(), -90)
			dangle:RotateAroundAxis(dangle:Forward(), 180)

			-- set up positioning
			dpos = dpos + dangle:Up() * ((door_thickness / 10))
			dpos = dpos - dangle:Right() * 12
			dpos = dpos - dangle:Forward() * (door_width - 1.6)
		elseif (side == "back") then
			dangle:RotateAroundAxis(dangle:Right(), 180)
			dangle:RotateAroundAxis(dangle:Up(), 0)
			dangle:RotateAroundAxis(dangle:Forward(), 0)

			-- set up positioning
			dpos = dpos + dangle:Up() * ((door_thickness / 10))
			dpos = dpos - dangle:Right()  * 12
			dpos = dpos - dangle:Forward() 
		end

		-- start drawing 2d in 3d
		cam.Start3D2D(dpos, dangle, 1/RenderScaleMult)

		--surface.SetDrawColor(255,255,255, 200)
		--surface.SetFont("GModRPFontOne")
		--surface.SetTextColor(255,0,0)
		--surface.SetTextPos(0,0)
		--surface.DrawRect(0,0,door_width * 10 - 5,48)

		local doorfont = "GModRPDoorFont"
		local doorwidth_display = door_width * RenderScaleMult

		local doorwidth_actual = door_width * RenderScaleMult - 10
		if (door_owner == false) then 
			local _,h = DrawShadowedText("Not Owned", doorfont, doorwidth_display / 2, 0, doorcolor_bad, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
			DrawShadowedText("Press F4 to rent!", doorfont, doorwidth_display / 2, h, doorcolor_info, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
		else 
			local _,h = DrawShadowedText("Door rented by", doorfont, doorwidth_actual / 2, 0, doorcolor_info, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
			DrawShadowedText(door_owner, doorfont, doorwidth_actual / 2, 0 + h, doorcolor_good, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)

		end
		-- debug rotation
		--DrawShadowedText(minB, doorfont, doorwidth_display / 2, 200, doorcolor_info, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
		--DrawShadowedText(maxB, doorfont, doorwidth_display / 2, 250, doorcolor_info, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)

		cam.End3D2D()

	end
	local unownable = door:GetNW2Bool("ent_unownable")
	if unownable == true then
		return
	end
	door_owner = door:GetEntOwner()
	if (door_owner) then
		door_owner = door_owner:Nick()
	end

	drawDoorSide("front")
	drawDoorSide("back")
end

function GM:PostDrawOpaqueRenderables(depth, skybox)
	GMRP.DrawDoors()
end

function GMRP.FindNearbyDoors()
	global_doors = {}
	for k,v in pairs( ents.FindInSphere(player_pos, door_max_dist*1.1) ) do
		if (v:isOwnable()) then
			table.insert(global_doors, v)
		end
	end
end

net.Receive("SBRPPacket", function(len)
	local pName = net.ReadString()
	if pName == "CHAT" then
		local chatTable = net.ReadTable()
		chat.AddText(unpack(chatTable))
		chat.PlaySound()
	elseif pName == "INVENTORY_ITEM" then
		MyInventory[net.ReadString()] = net.ReadString()
	elseif pName == "NOTIFY" then
		local str = net.ReadString()
		local color = net.ReadColor()
		Notify(str, color)
	elseif pName == "CROSSHAIR_MSG" then
		local str = net.ReadString()
		local color = net.ReadColor()
		local silkicon = net.ReadString()
		CrosshairMsg(str, color, silkicon)
	elseif pName == "INVENTORY_RESET" then
		MyInventory = {}
	end
end)

timer.Create("DoorFinder", .2, 0, function()
	GMRP.FindNearbyDoors()
end)