local chatsounds = {}

chatsounds["lol"] = "vo/Citadel/br_laugh01.wav"
chatsounds["rofl"] = "vo/ravenholm/madlaugh03.wav"
chatsounds["lmao"] = "vo/ravenholm/madlaugh03.wav"	
chatsounds["gtfo"] = "vo/npc/male01/gethellout.wav"	
chatsounds["hello"] = "vo/npc/male01/hi02.wav"
chatsounds["oh shit"] = "vo/npc/Barney/ba_ohshit03.wav"
chatsounds["omg"] = "vo/NovaProspekt/al_ohmygod.wav"
chatsounds["fuck you"] = "vo/Streetwar/rubble/ba_tellbreen.wav"
chatsounds["eek"] = "ambient/voices/f_scream1.wav"
chatsounds["oh yeah"] = "vo/npc/Barney/ba_ohyeah.wav"
chatsounds["rise and shine"] = "vo/gman_misc/gman_riseshine.wav"
chatsounds["hacks"] = "vo/npc/male01/hacks01.wav"
chatsounds["oh no"] = "vo/npc/male01/ohno.wav"	


function GMRP.ChatSounds( ply, text )

	for k, v in pairs( chatsounds ) do
	
		if string.find( string.lower( text ), k ) then
		
			ply:EmitSound( v )
			
		end
		
	end
	
end
	
hook.Add("PlayerSay", "GModRPChatSounds", GMRP.ChatSounds)


function GMRP.PrintChatSoundList( ply )
	
	for k, v in pairs( chatsounds ) do
	
		ply:PrintMessage( HUD_PRINTCONSOLE, k )	
		
	end
	
end	

GMRP.command( "chatsoundlist", GMRP.PrintChatSoundList)