if (!HUD) then HUD = false else HUD = HUD end

InvalidateHUDLayout = true

function hudDrawIcon(mat, x, y, w, h)
	surface.SetMaterial( mat )
	surface.SetDrawColor(100,100,100,100)
	surface.DrawTexturedRect( x+1,y+2,w,h ) 
	surface.SetDrawColor( 240, 240, 240)
	surface.DrawTexturedRect( x,y,w,h ) 
end

function GMRP.InitHud()
	if (!HUD) then
		local lHUD = vgui.Create("GMRPFrame")
		lHUD:ShowCloseButton( false )
		lHUD:SetPos(15, ScrH() - lHUD:GetTall() - 15)
		HUD = lHUD
	end
	if (InvalidateHUDLayout) then
		InvalidateHUDLayout = false
		HUD:SetSize(300, 195)
		HUD:SetPos(15, ScrH() - HUD:GetTall() - 15)
		HUD:SetZPos(32000)
		HUD:SetBackgroundBlur( true )
		HUD:SetPaintedManually( true ) 
		function HUD:PaintOver(w,h)
			surface.DisableClipping(false)
			local person = LocalPlayer() 

			-- HUD elements like HP below here ;)
			local numHealth = person:Health()
			local numEnergy = person:GetNWInt( "energy" )
			local numMoney = GMRP.FormatMoney(person:GetNWInt( "tehmoneylol" ))
			local numStamina = person:GetNWInt( "stamina" )

			surface.SetFont( "GModRPFontOne" )
			
			local trace = {}
			trace.start = person:EyePos()
			trace.endpos = trace.start + person:GetAimVector() * 85
			trace.filter = person
			local tr = util.TraceLine( trace )		
			
			local ent = tr.Entity
			
			-- height of each line in hud
			local hudHeight = 32

			-- unused now
			local hudPos = 15

			-- size of icons
			local iconSize = 30

			-- something
			local hudEPos = 0

			-- Y position of hud elements, starting from the top
			local hudYPos = HUD.TitleBarH

			-- X position of hud elements, starting from the left
			local hudXPos = 5

			-- anonymous function to calculate x pos for icon
			local iconYPos = function() 
				return hudYPos
			end

			local textYPos = function()
				return hudYPos + ((iconSize / 2) - (draw.GetFontHeight("GModRPFontOne") / 2))
			end

			local PercBarWidth = function(perc)
				if (perc == nil) then return 0 end

				local MaxWidth = w - iconSize - (hudXPos * 5)
				if (0 > 99) then 
					return MaxWidth 
				end
				if (perc < 1) then return 0 end
				return MaxWidth * (perc / 100)
			end

			local BarWidth = function()
				return PercBarWidth(100)
			end

			local DrawBar = function(num, max, colbg, colfg)
				surface.SetDrawColor( colbg )
				surface.DrawRect( hudXPos + iconSize + 10, iconYPos() + (iconSize / 4), BarWidth() , iconSize / 2 )

				surface.SetDrawColor( colfg )
				surface.DrawRect( hudXPos + iconSize + 10, iconYPos() + (iconSize / 4), PercBarWidth(num) , iconSize / 2 )
			end

			-- unused now
			local hudIPos = (ScrH() - hudHeight) + ((hudHeight - iconSize)/2) + 1
			
			-- Draw the hud box
			
			-- Job Information
			local job = person:GetJob()

			if job then
				surface.SetTextPos( hudXPos + 5, hudYPos + 5 )
				surface.SetTextColor(job.JobColor)
				surface.SetFont("GModRPFontOne")
				surface.DrawText( job.Title )
			end

			hudYPos = hudYPos + hudHeight

			-- HP ICON
			hudDrawIcon(Material("sandboxrp/icons/iheart.png"), hudXPos, iconYPos(), iconSize, iconSize)
			
			-- HP BACKGROUND BAR

			DrawBar(numHealth, 100, Color(50, 0, 0, 255), Color(240, 30, 30, 255))
			
			-- INCREMENT Y POS
			hudYPos = hudYPos + hudHeight
			
			-- HUNGER ICON
			hudDrawIcon(Material("sandboxrp/icons/ifood.png"), hudXPos, iconYPos(), iconSize, iconSize)
			
			-- HUNGER TEXT
			DrawBar(numEnergy, 100, Color(50, 30, 0), Color(150, 120, 30))
			
			-- INCREMENT Y POS
			hudYPos = hudYPos + hudHeight
			
			-- SPRINT ENERGY ICON
			hudDrawIcon(Material("sandboxrp/icons/ienergy.png"), hudXPos, iconYPos(), iconSize, iconSize)
			
			-- SPRINT ENERGY TEXT
			DrawBar(numStamina, 100, Color(50,50,0), Color(230,230,50))

			-- INCREMENT Y POS
			hudYPos = hudYPos + hudHeight
			
			-- MONEY ICON
			hudDrawIcon(Material("sandboxrp/icons/ibank.png"), hudXPos, iconYPos(), iconSize, iconSize)
			
			-- MONEY TEXT
			surface.SetTextColor( 255, 255, 255, 255 )
			surface.SetFont( "GModRPFontOne" )
			surface.SetTextPos( hudXPos + iconSize + 10, textYPos() )
			surface.DrawText( numMoney )	

			-- INCREMENT Y POS
			hudYPos = hudYPos + hudHeight

		end
	end
	HUD:SetTitle(LocalPlayer():GetName())

end

hook.Add( "PreDrawHUD", "HUDDraw", function()
	cam.Start2D()
	GMRP.InitHud()
	HUD:PaintManual() 
	cam.End2D()
end )