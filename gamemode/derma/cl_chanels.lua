--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]
   
 
function GMRP.Chanels()


	local RadioCFrame = vgui.Create("GMRPFrame")
	RadioCFrame:SetSize(700, 700) 
	RadioCFrame:SetTitle("Radio Channels") 
	RadioCFrame:SetPos(ScrW() / 2 - RadioCFrame:GetWide() / 2, ScrH() / 2 - RadioCFrame:GetTall() / 2) --:O
	RadioCFrame:SetVisible( true )
	RadioCFrame:SetDraggable( true )
	RadioCFrame:SetBackgroundBlur(true)
	RadioCFrame:ShowCloseButton(true)
	RadioCFrame:DrawList()
	RadioCFrame:MakePopup() 
	RadioCFrame:List_Spacing( 5 )
	RadioCFrame:List_Padding( 5 )
	
	
	for k, v in pairs( Radiostations ) do
	
		local Chanel = vgui.Create("DButton")
		Chanel:SetSize(120, 15)
		Chanel:SetText(v)
		Chanel.DoClick = function()
			RunConsoleCommand("gm_rp_radio_changechanel", v)
		end
		
		RadioCFrame:List_AddItem(Chanel)
		
	end	
	
	
end