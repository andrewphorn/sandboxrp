--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


local ScoreFrame
 
function GM:ScoreboardShow()


	ScoreFrame = vgui.Create("GMRPFrame")
	ScoreFrame:SetSize(700, 760) 
	ScoreFrame:SetTitle("Players")
	ScoreFrame:SetPos(ScrW() / 2 - ScoreFrame:GetWide() / 2, ScrH() / 2 - ScoreFrame:GetTall() / 2) --:O
	ScoreFrame:SetVisible(true)
	ScoreFrame:SetDraggable(true)
	ScoreFrame:SetBackgroundBlur(true)
	ScoreFrame:ShowCloseButton(false)
	ScoreFrame:MakePopup()
	
	
	local PlayerList = vgui.Create("DPanelList", ScoreFrame)
	PlayerList:SetPos(20, 50)
	PlayerList:SetSize(ScoreFrame:GetWide() - 40, ScoreFrame:GetTall() - 40)
	PlayerList:EnableVerticalScrollbar(true) 
	PlayerList:EnableHorizontal(false) 
	PlayerList:SetSpacing(10)
	PlayerList:SetPadding(0)


	for _, player in pairs(player.GetAll()) do
	
		local job = player:GetJob()
		local PlayerPanel = vgui.Create("DPanel")
		PlayerPanel:SetTall(68)
		PlayerPanel.Paint = function()
			surface.SetDrawColor(0,0,0,200)
			surface.DrawRect( 0, 0, PlayerPanel:GetWide(), PlayerPanel:GetTall() )		
	
		end
		PlayerList:AddItem(PlayerPanel)

		
		PlayerPanel.Icon = vgui.Create("SpawnIcon", PlayerPanel)
		PlayerPanel.Icon:SetModel(player:GetModel())
		PlayerPanel.Icon:SetDisabled(true)
		PlayerPanel.Icon:SetPos((PlayerPanel:GetTall() - PlayerPanel.Icon:GetTall()) / 2, (PlayerPanel:GetTall() - PlayerPanel.Icon:GetTall()) / 2)
		PlayerPanel.Icon:SetToolTip( nil )
		PlayerPanel.Icon.IsHovered = function()
		    return false 
		end
	
		PlayerPanel.Title = vgui.Create("DLabel", PlayerPanel)
		PlayerPanel.Title:SetPos( 90, PlayerPanel:GetTall() *.33 - PlayerPanel.Title:GetTall() / 2 )
		PlayerPanel.Title:SetText(player:Nick())
		PlayerPanel.Title:SetFont("GModRPContentFont")
		PlayerPanel.Title:SetColor( Color( 200, 200, 200, 255 ) )
		PlayerPanel.Title:SizeToContents() 
 		PlayerPanel.Title:SetContentAlignment( 5 )

 		local x,y = PlayerPanel.Title:GetPos()
 		PlayerPanel.Job = vgui.Create("DLabel", PlayerPanel)
 		PlayerPanel.Job:SetPos(x,y+PlayerPanel.Title:GetTall())
 		PlayerPanel.Job:SetText(job.Title)
 		PlayerPanel.Job:SetFont("GModRPContentFont")
 		PlayerPanel.Job:SetColor( job.JobColor )
 		PlayerPanel.Job:SizeToContents()
		
	end
	
	return true
	
end


function GM:ScoreboardHide()

	ScoreFrame:Close()
	return true
	
end