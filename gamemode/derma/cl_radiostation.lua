--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


local MusicForm

function GMRP.AddRadioStationCats(name, dir, dir2)

	local WowRadioCat = vgui.Create("DCollapsibleCategory")
	WowRadioCat:SetTall(50)
	WowRadioCat:SetExpanded(0) 
	WowRadioCat:SetLabel(name)


 	local WowRadio = vgui.Create("DPanelList")
	WowRadio:SetSpacing(5)
	WowRadio:SetPadding(5)
	WowRadio:SetAutoSize(true) 
	WowRadio:EnableVerticalScrollbar(false) 
	WowRadio:EnableHorizontal(false)

	WowRadioCat:SetContents(WowRadio)
	

	for k, v in pairs(file.Find(dir, "GAME")) do
		local Song = vgui.Create("DButton")
		Song:SetSize(120, 15)
		Song:SetText(v)
		Song.DoClick = function()
			RunConsoleCommand("gm_rp_radiostation_sound", dir2..v)
		end
		WowRadio:AddItem(Song)
	end	
	
	MusicForm:AddItem(WowRadioCat)	

end	



function GMRP.RadioStationMenu()

	local RadioFrame = vgui.Create("GMRPFrame")
	RadioFrame:SetSize(700, 700) 
	RadioFrame:SetTitle("") 
	RadioFrame:SetPos(ScrW() / 2 - RadioFrame:GetWide() / 2, ScrH() / 2 - RadioFrame:GetTall() / 2) --:O
	RadioFrame:SetVisible( true )
	RadioFrame:SetDraggable( true )
	RadioFrame:SetBackgroundBlur(true)
	RadioFrame:ShowCloseButton(true)	
	RadioFrame:DrawList()
	RadioFrame:MakePopup()
	RadioFrame:List_Spacing( 5 )
	RadioFrame:List_Padding( 5 )	
	
	
	RadioFrame:AddButton("Stop music", "gm_rp_radiostation_stop")


	MusicForm = vgui.Create("DForm")
	MusicForm:SetPadding(10)
	MusicForm:SetName("Sounds")
	RadioFrame:List_AddItem(MusicForm)	
	
	
	GMRP.AddRadioStationCats( "Music", "sound/music/*.mp3", "music/")	
	
	GMRP.AddRadioStationCats( "Voice", "sound/npc/overwatch/radiovoice/*", "npc/overwatch/radiovoice/")
	
	GMRP.AddRadioStationCats( "News etc", "sound/vo/Breencast/*", "vo/Breencast/")
	
	
end	
