--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


function GMRP.BookMenu( name )

	local person = LocalPlayer()

	local trace = {}
	trace.start = person:EyePos()
	trace.endpos = trace.start + person:GetAimVector() * 85
	trace.filter = person
	local tr = util.TraceLine( trace )	

	if tostring( name ) == nil || !tr.Entity:IsValid() then return end
	
	local BookFrame = vgui.Create("GMRPFrame")
	BookFrame:SetSize(700, 700) 
	BookFrame:SetTitle("") 
	BookFrame:SetPos(ScrW() / 2 - BookFrame:GetWide() / 2, ScrH() / 2 - BookFrame:GetTall() / 2) --:O
	BookFrame:SetVisible( true )
	BookFrame:SetDraggable( true )
	BookFrame:SetBackgroundBlur(true)
	BookFrame:ShowCloseButton(true)	
	BookFrame:MakePopup()
	BookFrame:DrawList()
	BookFrame:List_Spacing( 5 )
	BookFrame:List_Padding( 30 )
	
	BookFrame.Text = vgui.Create( "HTML" )
	BookFrame.Text:OpenURL( "http://www.plane-builders.com/books/"..tostring( name )..".html" )
	BookFrame.Text:SetTall( 450 )
	
	BookFrame:List_AddItem( BookFrame.Text )
	
end	