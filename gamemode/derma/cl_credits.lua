--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]
   
 
function GMRP.Credits()

	local CreditsFrame = vgui.Create( "GMRPFrame" )
	CreditsFrame:SetSize( 700, 700 ) 
	CreditsFrame:SetTitle( "Credits" ) 
	CreditsFrame:SetPos( ScrW() / 2 - CreditsFrame:GetWide() / 2, ScrH() / 2 - CreditsFrame:GetTall() / 2 ) --:O
	CreditsFrame:SetVisible( true )
	CreditsFrame:SetDraggable( true )
	CreditsFrame:SetBackgroundBlur( true )
	CreditsFrame:ShowCloseButton( true )
	CreditsFrame:MakePopup() 


	local DermaListView = vgui.Create( "DListView" )
	DermaListView:SetParent( CreditsFrame )
	DermaListView:SetPos( 150,40 )
	DermaListView:SetSize( CreditsFrame:GetWide() - 170, CreditsFrame:GetTall() - 60 )
	DermaListView:SetMultiSelect( false )
	DermaListView:AddColumn(  "Name" )
	DermaListView:AddColumn( "Info" ) 
	DermaListView:AddLine( "Slob187", "Original: Gamemode creator." )
	DermaListView:AddLine( "Biohazard", "Original: Movies, website owner." )
	DermaListView:AddLine( "Computid", "Original: Web developer." )
	DermaListView:AddLine( "Snake Doctor", "Original: Photoshoper, web developer." )
	DermaListView:AddLine( "AndrewPH", "SandboxRP: Programmer, maintainer")

end

GMRP.command( "credits", GMRP.Credits )