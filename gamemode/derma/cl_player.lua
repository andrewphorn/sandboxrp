--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


MyInventory = MyInventory or {}
MyPermits = MyPermits or {}


local PlayerCommandList

local function AddPlayerCommandButton( name, command )
	
	local PCButton = vgui.Create("DButton", RadioFrame)
	PCButton:SetTall( 20 )
	PCButton:SetText( name )
	PCButton.DoClick = function()
	
		LocalPlayer():ConCommand( command )
		
	end
	PlayerCommandList:AddItem( PCButton )
	
end


function GMRP.PlayerPanel()

	local PlayerPanel = vgui.Create("GMRPFrame")
	PlayerPanel:SetSize(700, 700) 
	PlayerPanel:SetTitle(LocalPlayer():Nick()) 
	PlayerPanel:SetPos(ScrW() / 2 - PlayerPanel:GetWide() / 2, ScrH() / 2 - PlayerPanel:GetTall() / 2) --:O
	PlayerPanel:SetVisible( true )
	PlayerPanel:SetDraggable( true )
	PlayerPanel:SetBackgroundBlur(true)
	PlayerPanel:ShowCloseButton(true)
	PlayerPanel:MakePopup()
	
		
	local PlayerSheet = vgui.Create( "DPropertySheet", PlayerPanel )
	PlayerSheet:Dock(FILL)
	PlayerSheet:DockMargin(5,5,5,5)

	local JobList = vgui.Create( "DPanelList" )
	JobList:SetSpacing( 1 )
	JobList:SetPadding( 1 )
	JobList:EnableVerticalScrollbar( true )
	JobList:EnableHorizontal( false )
	JobList:SetWide(PlayerPanel:GetWide() - 40)

	for jobid, Job in pairs(GMRP.Jobs) do
		local JobPanel = vgui.Create("DPanel")
		JobPanel:SetTall(60)
		JobList:AddItem(JobPanel)

		local Title = vgui.Create("DLabel", JobPanel)
		Title:SetPos(15,10)
		Title:SetTextColor(Job.JobColor)
		Title:SetText(Job.Title)
		Title:SetFont("GModRPContentFont")
		Title:SizeToContents()
		Title:SetContentAlignment( 5 )

		local Description = vgui.Create("DLabel", JobPanel)
		Description:SetPos(15,Title:GetPos() + Title:GetTall())
		Description:SetTextColor(Color(80,80,80,255))
		Description:SetText(Job.Description)
		Description:SetFont("GModRPContentFont")
		Description:SizeToContents()
		Description:SetContentAlignment( 5 )
		JobPanel:SizeToContents()
		JobPanel:SetTall(JobPanel:GetTall() + 10)

		local ChangeJob = vgui.Create("DButton", JobPanel)
		ChangeJob:SetSize(80,30)
		ChangeJob:SetText("Choose")
		ChangeJob:Dock(RIGHT)
		if (jobid == LocalPlayer():GetJob().ID) then
			ChangeJob:SetDisabled( true )
			ChangeJob:SetText("Unavailable")
		end

		JobPanel.Paint = function()
			surface.SetDrawColor(Job.JobColor)
			surface.DrawRect(0, 0, 4, JobPanel:GetTall())

			surface.SetDrawColor(0,0,0,200)
			local x,y = Title:GetPos()
			draw.RoundedBox(8, x-7, y-5, Title:GetWide()+12, Title:GetTall()+8, Color(0,0,0,150))
		end

		ChangeJob.DoClick = function()
			RunConsoleCommand("gm_rp_setjob", jobid)
			PlayerPanel:Close()
		end

	end

	PlayerSheet:AddSheet( "Jobs", JobList, "icon16/money.png", false, false, "Choose a job here!" )	

	local BuyPermitList = vgui.Create( "DPanelList" )
	BuyPermitList:SetSpacing( 1 )
	BuyPermitList:SetPadding( 1 )
	BuyPermitList:EnableVerticalScrollbar( true ) 
	BuyPermitList:EnableHorizontal( false )	
	
	for k, v in pairs( Permits ) do
	
		local PermitPanel = vgui.Create("DPanel")
		PermitPanel:SetTall(75)
		PermitPanel.Paint = function()
  			surface.SetDrawColor(180, 180, 180, 255) 
			surface.DrawRect(0, 0, PermitPanel:GetWide(), PermitPanel:GetTall())
		end
		BuyPermitList:AddItem(PermitPanel)

		
		PermitPanel.Icon = vgui.Create("SpawnIcon", PermitPanel)
		PermitPanel.Icon:SetModel("models/props_lab/clipboard.mdl")
		PermitPanel.Icon:SetPos(3, 3)

		
		local Title = vgui.Create("DLabel", PermitPanel)
		Title:SetPos(90, 10)
		Title:SetText(Permits[k][1])
		Title:SetFont("GModRPContentFont")
		Title:SetColor( Color( 90, 111, 142, 225 ) )
		Title:SizeToContents() 
 		Title:SetContentAlignment( 5 )				
		
		local Price = vgui.Create( "DLabel", PermitPanel )	
		Price:SetPos(90, 40)
		Price:SetText( "Price: "..Permits[k][2] )
		Price:SetFont( "GModRPContentFont" )
		Price:SetColor( Color( 90, 111, 142, 225 ) )
		Price:SizeToContents() 
 		Price:SetContentAlignment( 5 )				
		
		local pbPos = 470
		
		PermitPanel.Buy = vgui.Create( "DButton", PermitPanel ) 
		PermitPanel.Buy:SetSize( 60, 20 )
		PermitPanel.Buy:SetPos( pbPos, PermitPanel:GetTall()/2-10 ) 
		PermitPanel.Buy:SetText( "Purchase" )
		
		if ( LocalPlayer():HasPermit( k ) ) then
		
			PermitPanel.Buy:SetDisabled( true )
			
		end
		
		PermitPanel.Buy.DoClick = function()
	
			RunConsoleCommand( "gm_rp_buypermit", k )
			PlayerPanel:Close()
			
		end			
		
		pbPos = pbPos + 80		
		
		PermitPanel.Drop = vgui.Create( "DButton", PermitPanel ) 
		PermitPanel.Drop:SetSize( 60, 20 )
		PermitPanel.Drop:SetPos( pbPos, PermitPanel:GetTall()/2-10 ) 
		PermitPanel.Drop:SetText( "Drop" )
		
		if ( !LocalPlayer():HasPermit( k ) ) then
		
			PermitPanel.Drop:SetDisabled( true )
			
		end		
		
		PermitPanel.Drop.DoClick = function()
		
			RunConsoleCommand( "gm_rp_permit_drop", k )
			PlayerPanel:Close()
				
		end					

	end		

	PlayerSheet:AddSheet( "Upgrades", BuyPermitList, "icon16/award_star_add.png", false, false, "Buy upgrades here" )	
	-- Variable containing all playermodels: citizenmodels

	--[[
		APPEARANCE EDITOR
	]]--
	local PlayersColor = LocalPlayer():GetPlayerColor()

	local function CtoV(col)
		return Vector(col.r / 255, col.g / 255, col.b / 255)
	end

	local function VtoC(vect)
		return Color(vect[1]*255, vect[2]*255, vect[3]*255)
	end

	local ModelPicker = vgui.Create("DPanel")
	function ModelPicker:Paint(w,h) end
	local ModelPickerRight = vgui.Create("DPanel", ModelPicker)
	ModelPickerRight:SetWide(PlayerPanel:GetWide()/3)
	ModelPickerRight:Dock(RIGHT)

	--[[
		MODEL PREVIEW
	]]--
	local ModelView = vgui.Create("DModelPanel", ModelPicker )
	local ChosenColor = VtoC(PlayersColor)
	ModelView:Dock(FILL)
	ModelView:SetModel(LocalPlayer():GetModel())
	ModelView:SetFOV(20)
	local headpos = ModelView.Entity:GetBonePosition( ModelView.Entity:LookupBone( "ValveBiped.Bip01_Spine2" ) )
	ModelView:SetCamPos(headpos-Vector(-100,30,0))
	ModelView:SetLookAt(headpos)
	local function GetPlayerColor()
		return CtoV(ChosenColor)
	end
	function ModelView:LayoutEntity() end
	ModelView.Entity.GetPlayerColor = GetPlayerColor

	--[[
		LOCK IN BUTTON
	]]--
	local ModelSaveButton = vgui.Create("DButton", ModelView)
	ModelSaveButton:Dock(BOTTOM)
	ModelSaveButton:DockMargin(10,10,10,10)
	ModelSaveButton:SetText("Save Settings")
	function ModelSaveButton:DoClick()
		RunConsoleCommand( "gm_rp_setmodel", ModelView:GetModel() )
		local plcol = ModelView.Entity:GetPlayerColor()
		RunConsoleCommand( "gm_rp_setmodelcolor", plcol[1]*255, plcol[2]*255, plcol[3]*255)
	end

	--[[
		MODEL LIST
	]]--
 	local ModelList = vgui.Create( "DPanelList", ModelPickerRight )
	ModelList:SetSpacing( 5 )
	ModelList:SetPadding( 3 )
	ModelList:Dock(FILL)
	ModelList:DockMargin(5,5,5,5)
	ModelList:EnableVerticalScrollbar( true ) 
	ModelList:EnableHorizontal( true )

	--ModelPickerRight:AddSheet("Models", ModelList, "icon16/user_edit.png", false, false, "Choose your model")

	--[[
		COLOR PICKER
	]]--
	local ModelColorPicker = vgui.Create( "DColorMixer", ModelPickerRight )
	ModelColorPicker:Dock(BOTTOM)
	ModelColorPicker:SetColor(VtoC(PlayersColor))
	ModelColorPicker:DockMargin(5,0,5,5)
	function ModelColorPicker:ValueChanged(col)
		ChosenColor = col
	end

	--ModelPickerRight:AddSheet("Color", ModelColorPicker, "icon16/user_edit.png", false, false, "Choose your color")

	--[[function ModelView.Entity:GetPlayerColor() 
		return ChosenColor
	end]]


	for k, v in pairs( citizenmodels ) do
	
		local ModelIcon = vgui.Create( "SpawnIcon" )
		ModelIcon:SetModel( v )		
		ModelIcon.DoClick = function()
			
			ModelView:SetModel( v )
			ModelView.Entity.GetPlayerColor = GetPlayerColor
			
		end
		
		ModelList:AddItem( ModelIcon )
		
	end
	
	PlayerSheet:AddSheet( "Appearance", ModelPicker, "icon16/user_edit.png", false, false, "Select your player model here." )
		
	local InventoryList = vgui.Create( "DPanelList" )
	InventoryList:SetSpacing( 5 )
	InventoryList:SetPadding( 1 )
	InventoryList:EnableVerticalScrollbar( true ) 
	InventoryList:EnableHorizontal( false )		


	for itemname, item in pairs( GMRP.Items ) do
		
		for k, v in pairs( MyInventory ) do
		
			if k == itemname then

				local InventorySpot = vgui.Create("DPanel")
				InventorySpot:SetSize(InventoryList:GetWide(), 100)
				function InventorySpot:Paint(w,h)
					surface.SetDrawColor(0,0,0,120)
					surface.DrawRect(0,0,w,h)
				end
		
				local InventoryIcon = vgui.Create( "DModelPanel", InventorySpot)
				InventoryIcon:Dock(LEFT)
				InventoryIcon:SetWide(110)
				InventoryIcon:SetModel( item.Model )
				local ent = InventoryIcon:GetEntity()
				local mn, mx = ent:GetRenderBounds()
				local size = 0
				size = math.max( size, math.abs( mn.x ) + math.abs( mx.x ) )
				size = math.max( size, math.abs( mn.y ) + math.abs( mx.y ) )
				size = math.max( size, math.abs( mn.z ) + math.abs( mx.z ) )

				InventoryIcon:SetFOV(48)
				InventoryIcon:SetCamPos( Vector(size, size, size) )
				InventoryIcon:SetLookAt( (mn + mx) *.5 )
				ent:SetPos(ent:GetPos() - Vector(0,0,-5))
				function InventoryIcon:LayoutEntity() end

				-- add some buttons
				local InventoryItemBtns = vgui.Create("DPanel", InventorySpot)
				InventoryItemBtns:Dock(RIGHT)
				InventoryItemBtns:DockMargin(15,15,15,15)
				function InventoryItemBtns:Paint(w,h) end

				-- add drop button
				local InventoryItemDrop = vgui.Create("DButton", InventoryItemBtns)
				InventoryItemDrop:Dock(TOP)
				InventoryItemDrop:SetTall(30)
				InventoryItemDrop:SetText("Drop")

				-- add container for name/qty
				local InventoryItemNameSpot = vgui.Create("DPanel", InventorySpot)
				InventoryItemNameSpot:Dock(TOP)
				InventoryItemNameSpot:DockMargin(15,15,15,15)
				function InventoryItemNameSpot:Paint(w,h) end
				
				-- add label (name)
				local InventoryItemName = vgui.Create("DLabel", InventoryItemNameSpot)
				InventoryItemName:Dock(LEFT)
				InventoryItemName:SetFont("GModRPContentFont")
				InventoryItemName:SetTextColor(Color(230,230,230))
				InventoryItemName:SetText(item.Name)
				InventoryItemName:SizeToContents()

				-- add quantity
				local InventoryItemQty = vgui.Create("DLabel", InventoryItemNameSpot)
				InventoryItemQty:Dock(LEFT)
				InventoryItemQty:SetFont("GModRPContentFont")
				InventoryItemQty:SetTextColor(Color(230,230,230,110))
				InventoryItemQty:SetText(" x"..MyInventory[k].." ")
				InventoryItemQty:SizeToContents()

				-- add description
				local InventoryItemDescription = vgui.Create("DLabel", InventorySpot)
				InventoryItemDescription:Dock(FILL)
				InventoryItemDescription:DockMargin(15,15,15,15)
				InventoryItemDescription:SetFont("GModRPUIContentFont")
				InventoryItemDescription:SetText(item.Info or "")

				-- add click stuff for drop btn
				function InventoryItemDrop:DoClick()
					if !GMRP.UseLimit( LocalPlayer() ) then return end
					local newqty = MyInventory[k] - 1
					if newqty == 0 then 
						InventorySpot:Remove() 
					end
					InventoryItemQty:SetText(" x"..newqty.." ")
					RunConsoleCommand( "gm_rp_inventory_drop", k )	
				end


				if item.Remove then -- add use button
					local InventoryItemUse = vgui.Create("DButton", InventoryItemBtns)
					InventoryItemUse:Dock(BOTTOM)
					InventoryItemUse:SetTall(30)
					InventoryItemUse:SetText("Use")

					-- click stuff for use btn
					function InventoryItemUse:DoClick()
						if !GMRP.UseLimit( LocalPlayer() ) then return end

						local newqty = MyInventory[k] - 1
						if newqty == 0 then 
							InventorySpot:Remove() 
						end
						InventoryItemQty:SetText(" x"..newqty.." ")
						RunConsoleCommand( "gm_rp_inventory_use", k )
					end
				end

				InventoryList:AddItem( InventorySpot )
				
			end	
			
		end
		
	end		
	

	PlayerSheet:AddSheet( "Inventory", InventoryList, "icon16/box.png", false, false, "Your items are stored here." )
	
	
	PlayerCommandList = vgui.Create( "DPanelList" )
	PlayerCommandList:SetSpacing( 2 )
	PlayerCommandList:SetPadding( 5 )
	PlayerCommandList:EnableVerticalScrollbar( true ) 
	PlayerCommandList:EnableHorizontal( false )			
	
	
	AddPlayerCommandButton( "Toggle sit", "gm_rp_sit" )	
	
	AddPlayerCommandButton( "Toggle sleep", "gm_rp_sleep" )
	
	AddPlayerCommandButton( "Drop weapon", "gm_rp_dropweapon" )	
	
	PlayerSheet:AddSheet( "Player commands", PlayerCommandList, "icon16/application_xp_terminal.png", false, false, "Player commands." )
	
end	

GMRP.command( "playermenu", GMRP.PlayerPanel )