--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]

   
local rules = 	{

				"!SandboxRP",
				"Simple roleplay, simple fun.",
				"",
				"!Rules",
				"Don't be a mingebag.",
				"Don't be an asshat",
				"Don't abuse the system",
				"",
				"!Items",
				"Pick up items in the world by pressing R.",
				"Use items in the world by pressing E.",
				"",
				"!Chat commands",
				"/drop <amount> - Drop amount of money specified",
				"// - Speak globally in out-of-character chat.",
				"",
				"!Hotkeys",
				"F1 - This menu",
				"F2 - Market Menu",
				"F3 - Player Menu",
				"F4 - Door interactions",				
				}  


function GMRP.Motd()


	local MotdFrame = vgui.Create("GMRPFrame")
	MotdFrame:SetSize(700, 700) 
	MotdFrame:SetTitle("MOTD") 
	MotdFrame:SetPos(ScrW() / 2 - MotdFrame:GetWide() / 2, ScrH() / 2 - MotdFrame:GetTall() / 2) --:O
	MotdFrame:SetVisible( true )
	MotdFrame:SetDraggable( true )
	MotdFrame:SetBackgroundBlur(true)
	MotdFrame:ShowCloseButton(true)
	MotdFrame:DrawList()
	MotdFrame:MakePopup() 
	MotdFrame:List_Spacing( 7 )
	MotdFrame:List_Padding( 2 )		

	--MotdFrame:AddButton( "Credits", "gm_rp_credits" )		
	
	for k, v in pairs( rules ) do
	
		local ReadThis = vgui.Create( "DLabel" )
		if (v:sub(1,1) == '!') then
			v = v:sub(2,#v)
			ReadThis:SetFont( "GModRPUITitleFont" )	
		else
			ReadThis:SetFont( "GModRPUIContentFont" )	
		end
		ReadThis:SetText( v )
		ReadThis:SizeToContents()
		MotdFrame:List_AddItem( ReadThis )
		
	end	

	local CreditsButton = vgui.Create("DButton")
	CreditsButton:SetSize(120,40)
	CreditsButton:SetText("Credits")
	CreditsButton.DoClick = function()
	
		LocalPlayer():ConCommand( "gm_rp_credits" )
		
	end
	MotdFrame:List_AddItem( CreditsButton )

end

GMRP.command( "motd", GMRP.Motd )