--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]

	
local ItemShit

function GMRP.AddItemCat( name, type, type2 )


	local Cat = vgui.Create( "DCollapsibleCategory" )
	Cat:SetTall( 50 )
	Cat:SetExpanded( 1 ) 
	Cat:SetLabel( name )


 	local CatList = vgui.Create( "DPanelList" )
	CatList:SetSpacing( 1 )
	CatList:SetPadding( 1 )
	CatList:SetAutoSize( true ) 
	CatList:EnableVerticalScrollbar( false ) 
	CatList:EnableHorizontal( true )

	Cat:SetContents( CatList )

	local catEmpty = true

	for itemname, item in pairs(GMRP.Items) do	
	
		if (item.Type == tostring( type ) or item.Type == tostring( type2 )) 
			and LocalPlayer():CanBuyItem(item) then
			
			catEmpty = false
			local ItemIcon = vgui.Create("SpawnIcon")
			ItemIcon:SetModel(item.Model)
			ItemIcon:SetToolTip("Name: "..item.Name.."\nPrice: "..CUR..item.Price.."\nDescription: "..item.Info)
			ItemIcon.DoClick = function()
			
				RunConsoleCommand("gm_rp_buyitem", itemname)
				
			end	
			
			CatList:AddItem( ItemIcon )
			
		end
		
	end	
	
	if !catEmpty then
		ItemShit:AddItem( Cat )
	else
		Cat:Remove()
	end
	
end	


function GMRP.OpenShop()


	local ShopFrame = vgui.Create("GMRPFrame")
	ShopFrame:SetSize(700, 700) 
	ShopFrame:SetTitle("Shop") 
	ShopFrame:SetPos(ScrW() / 2 - ShopFrame:GetWide() / 2, ScrH() / 2 - ShopFrame:GetTall() / 2) --:O
	ShopFrame:SetVisible( true )
	ShopFrame:SetDraggable( true )
	ShopFrame:SetBackgroundBlur(true)
	ShopFrame:ShowCloseButton(true)
	ShopFrame:DrawList()	
	ShopFrame:MakePopup()
	ShopFrame:List_Spacing( 5 )
	ShopFrame:List_Padding( 5 )	
			
	
	ItemShit = vgui.Create("DForm")
	ItemShit:SetPadding(24)
	ItemShit:SetName("Items")
	ShopFrame:List_AddItem(ItemShit)
	
	
	GMRP.AddItemCat( "Food and drinks", "food", "drink" )
	
	GMRP.AddItemCat( "Alcohol", "alcohol" )	
	
	GMRP.AddItemCat( "Plants", "plant" )
	
	GMRP.AddItemCat( "Health", "health" )	
	
	GMRP.AddItemCat( "Items", "item" )	
	
	GMRP.AddItemCat( "Books", "book" )	
	
	GMRP.AddItemCat( "Vehicles", "vehicle" )			
	
	GMRP.AddItemCat( "Black market", "blackmarket" )	

	GMRP.AddItemCat( "Special", "special" )	
	
end	


GMRP.command( "shopmenu", GMRP.OpenShop)	
