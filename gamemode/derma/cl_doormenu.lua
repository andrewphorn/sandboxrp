--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


GMRP.command( "door_menu_addowner", function()

	Derma_StringRequest( "Door", "Enter the door co-owner's name.", "Enter a part of co-owner's name here.", function( text )
		RunConsoleCommand( "gm_rp_door_addowner", text ) 
	end )

end	)


GMRP.command( "door_menu_removeowner", function()

	Derma_StringRequest( "Door", "Enter the name here.", "Enter a part of the player's name who you want to remove from door co-owners.", function( text )
		RunConsoleCommand( "gm_rp_door_removeowner", text ) 
	end )

end	)


local DoorFrame


local function AddDoorButton( name, command )
	
	local DoorButton = vgui.Create("DButton", RadioFrame)
	DoorButton:SetTall( 20 )
	DoorButton:SetText( name )
	DoorButton.DoClick = function()
		LocalPlayer():ConCommand( command )
	end
	DoorFrame:List_AddItem( DoorButton )
	
end


function GMRP.DoorMenu()


	DoorFrame = vgui.Create("GMRPFrame")
	DoorFrame:SetSize(700, 700) 
	DoorFrame:SetTitle("") 
	DoorFrame:SetPos(ScrW() / 2 - DoorFrame:GetWide() / 2, ScrH() / 2 - DoorFrame:GetTall() / 2) --:O
	DoorFrame:SetVisible( true )
	DoorFrame:SetDraggable( true )
	DoorFrame:SetBackgroundBlur(true)
	DoorFrame:ShowCloseButton(true)
	DoorFrame:DrawList()
	DoorFrame:MakePopup() 
	DoorFrame:List_Spacing( 5 )
	DoorFrame:List_Padding( 5 )		
	

	AddDoorButton( "Add owner", "gm_rp_door_menu_addowner" )
	
	AddDoorButton( "Remove owner", "gm_rp_door_menu_removeowner" )
	
	AddDoorButton( "Sell door", "gm_rp_door_sell" )
	
end

GMRP.command( "door_menu", GMRP.DoorMenu )