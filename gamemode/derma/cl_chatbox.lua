CHAT = CHAT or false
InvalidateChat = true
local msgqueue = {}
local defaultOpacity = 255
local last_vgui_draw = VGUIFrameTime()

function GMRP.InitChatbox()
	if !CHAT then
		local cbox = vgui.Create("GMRPFrame")
		defaultOpacity = cbox.BGOpacity
		cbox:ShowCloseButton( false )
		cbox:SetPos(30 + HUD:GetWide(), ScrH() - 15 - 195)
		CHAT = {}
		CHAT.frame = cbox
	end

	if InvalidateChat then
		InvalidateChat = false
		local padding = 5
		local input_height = 26
		local cbox = CHAT.frame
		cbox:SetSize(500, 195)
		cbox:SetBackgroundBlur( true )
		cbox:SetPaintedManually( true )
		cbox:SetZPos(32001) -- 1 more than HUD
		cbox:SetTitle("Chat")

		CHAT.frame = cbox

		-- set up RichText area
		local ChatLog = CHAT.log or vgui.Create("RichText", cbox)
		ChatLog:SetSize(cbox:GetWide() - (padding * 2), cbox:GetTall() - (padding * 2) - (input_height ) - cbox.TitleBarH)
		ChatLog:SetPos(padding, padding + cbox.TitleBarH)
		local chat_blur_fade = 3

		function ChatLog:Paint(w,h)
			surface.SetDrawColor(0,0,0,100)
			surface.DrawRect(0,0,w,h)
		end

		function ChatLog:Think() 
			local dt = VGUIFrameTime() - last_vgui_draw
			last_vgui_draw = VGUIFrameTime()
			if CHAT.input:IsVisible() && CHAT.frame.BGOpacity < defaultOpacity then
				CHAT.frame.BGOpacity = CHAT.frame.BGOpacity + ((255 / 3) * dt)
			elseif !CHAT.input:IsVisible() && CHAT.frame.BGOpacity > 0 then
				CHAT.frame.BGOpacity = CHAT.frame.BGOpacity - ((255 / 3) * dt)
			end
		end
		ChatLog:SetFontInternal("GModRPUIChatFont") 
		CHAT.log = ChatLog

		local InputBar = CHAT.input or vgui.Create("DTextEntry", cbox)
		InputBar:SetSize(cbox:GetWide() - padding * 2, input_height)
		InputBar:SetTextColor(Color(230,230,230)) 
		InputBar:SetFont("GModRPUIChatFont")
		InputBar:SetCursorColor(Color(255,255,255))
		InputBar:SetHighlightColor(Color(153, 192, 255))
		InputBar:SetDrawBackground( false )
		InputBar:SetPos(padding,cbox:GetTall() - padding -  input_height)
		function InputBar:Paint(w,h)
			if !self:IsVisible() then return end
			if gui.IsGameUIVisible() then
				chat.Close()
			end
			surface.SetDrawColor(0,0,0,200)
			surface.DrawRect(0,0,w,h)
			derma.SkinHook( "Paint", "TextEntry", self, w, h)
		end

		InputBar.OnTextChanged = function( self )
			if self and self.GetText then
				gamemode.Call( "ChatTextChanged", self:GetText() or "")
			end
		end

		InputBar.OnKeyCodeTyped = function( self, code )
			if code == KEY_ENTER || code == KEY_PAD_ENTER then
				if string.Trim( self:GetText() ) ~= "" then
					net.Start("SBRPPacket")
					net.WriteString("CHAT")
					net.WriteString(self:GetText())
					net.SendToServer()
				end
				CHAT.close()
			end
		end


		CHAT.input = InputBar

		CHAT.close = function()
			CHAT.log:GotoTextEnd()
			CHAT.log:SetVerticalScrollbarEnabled( false )
			CHAT.input:SetVisible( false )
			gamemode.Call( "FinishChat" )
			CHAT.input:SetText( "" )
			CHAT.frame:SetMouseInputEnabled( false )
			CHAT.frame:SetKeyboardInputEnabled( false )
			gui.EnableScreenClicker( false )
			gamemode.Call( "ChattextChanged", "" )
		end

		CHAT.open = function()
			CHAT.input:SetVisible( true )
			CHAT.log:SetVerticalScrollbarEnabled( true )
			CHAT.frame:MakePopup()
			CHAT.input:RequestFocus()
		end

		function cbox:PaintOver(w,h)

		end
		CHAT.close()
	end
end

function chat.AddText(...)
	GMRP.AddChat({...})
end

function GMRP.AddChat(mtable)
	if !CHAT || !CHAT.log then
		msgqueue[#msgqueue] = mtable
		return
	end

	if #msgqueue > 0 then
		for _,m in msgqueue do
			GMRP.AddChatText(m)
		end
	end
	GMRP.AddChatText(mtable)

end

function GMRP.AddChatText(mtable)
	local msg = {}
	for _, obj in pairs(mtable) do
		if type(obj) == "table" then
			CHAT.log:InsertColorChange(obj.r, obj.g, obj.b, obj.a)
			table.insert( msg, obj)
		elseif type(obj) == "string" then
			CHAT.log:AppendText(obj)
			table.insert(msg, obj)
		end
	end
	CHAT.log:SetFontInternal("GModRPUIChatFont") 
	CHAT.log:AppendText("\n")
	CHAT.log:InsertColorChange(255,255,255,255)
end

hook.Add("ChatText", "chatbox_joinleave", function(index, name, text, typ)
	if typ ~= "chat" then
		GMRP.AddChat({Color(132, 172, 244,255),text})
		return true
	end
end)


hook.Add( "PreDrawHUD", "ChatboxDraw", function()
	cam.Start2D()
	GMRP.InitChatbox()
	if CHAT.frame then
		CHAT.frame:PaintManual() 
	end
	cam.End2D()
end )

--// Stops the default chat box from being opened
hook.Remove("PlayerBindPress", "chatbox_hijack")
hook.Add("PlayerBindPress", "chatbox_hijack", function(ply, bind, pressed)
	if string.sub( bind, 1, 11 ) == "messagemode" then
		if bind == "messagemode2" then 
			CHAT.ChatType = "teamchat"
		else
			CHAT.ChatType = ""
		end
		
		chat.Open()
		return true
	end
end)

function chat.Close(...)
	if CHAT then
		CHAT.close()
	end
end

chat.Open = function(...)
	if CHAT then
		CHAT.open()
	end
end