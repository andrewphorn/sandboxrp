--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]
   
 
function GMRP.Donation()

	local DonationFrame = vgui.Create("GMRPFrame")
	DonationFrame:SetSize(700, 700) 
	DonationFrame:SetTitle("") 
	DonationFrame:SetPos( ScrW() / 2 - DonationFrame:GetWide() / 2, ScrH() / 2 - DonationFrame:GetTall() / 2 )
	DonationFrame:SetVisible( true )
	DonationFrame:SetDraggable( true )
	DonationFrame:SetBackgroundBlur(true)
	DonationFrame:ShowCloseButton(true)
	DonationFrame:DrawList()
	DonationFrame:MakePopup() 
	DonationFrame:List_Spacing( 5 )
	DonationFrame:List_Padding( 5 )	
		
	local dnlabel = vgui.Create("DLabel")
	dnlabel:SetText( "You can donate for gamemode's development here: slob187pb@yahoo.com" )
	dnlabel:SetFont( "GModRPFontOne" )		
	DonationFrame:List_AddItem( dnlabel )
	
end

GMRP.command( "donation", GMRP.Donation)