GMRP.Jobs = GMRP.Jobs or {}

GMRP.RegisterJob = function( JobTable ) 
	GMRP.Jobs[ JobTable.ID ] = {
		ID = JobTable.ID,
		Title = JobTable.Title or BaseJob.Title,
		Paycheck = JobTable.Paycheck or BaseJob.Paycheck,
		Description = JobTable.Description or BaseJob.Description,
		OnLoadout = JobTable.OnLoadout or false,
		JobColor = JobTable.JobColor or BaseJob.JobColor,
		TeamID = #team.GetAllTeams(),
		IsMerchant = JobTable.IsMerchant or BaseJob.IsMerchant
	}
end

-- define the 'base' job
BaseJob = {}

BaseJob.ID = "default"
BaseJob.Title = "Citizen"
BaseJob.Paycheck = 30
BaseJob.Description = "Live life to the fullest, become a citizen today!"
BaseJob.JobColor = Color(30, 160, 12)
BaseJob.IsMerchant = false

GMRP.RegisterJob(BaseJob)

function GMRP.JobExists(Job) 
	for jobid,_ in pairs(GMRP.Jobs) do
		if jobid == Job then
			return true
		end
	end
	return false
end

if (!SERVER) then return end

