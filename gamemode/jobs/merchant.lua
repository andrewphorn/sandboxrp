local JOB = JOB or {}

JOB.ID = "merchant"
JOB.Title = "Merchant"
JOB.Paycheck = 40
JOB.Description = "Sell goods for cash moneys."
JOB.JobColor = Color(232, 153, 34)
JOB.OnLoadout = false
JOB.IsMerchant = true

GMRP.RegisterJob(JOB)