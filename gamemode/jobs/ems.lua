local JOB = JOB or {}

JOB.ID = "ems"
JOB.Title = "Emergency Responder"
JOB.Paycheck = 50
JOB.Description = "Save lives, heal the hurt, and extinguish the enflamed."
JOB.JobColor = Color(226, 75, 61)
JOB.OnLoadout = function(p) 
	if p:HasPermit("firefighter") then
		p:Give("gmod_rp_firehose")
	end
	if p:HasPermit("healer") then
		--p:Give("gmod_rp_healgun")
	end
end

GMRP.RegisterJob(JOB)