local JOB = JOB or {}

JOB.ID = "officer"
JOB.Title = "Officer"
JOB.Paycheck = 60
JOB.Description = "Protect and swerve"
JOB.JobColor = Color(27, 113, 252)

function JOB.OnLoadout(p)
	p:Give( "gmod_rp_handcuffs" )

	if (p:HasPermit("police_battering")) then
		p:Give( "gmod_rp_door_ram" )
	end

	if (p:HasPermit("police_stunstick")) then
		p:Give( "weapon_stunstick" )
	end

	if (p:HasPermit("police_keys")) then
		p:Give( "gmod_rp_copkeys" )
	end
end

GMRP.RegisterJob(JOB)