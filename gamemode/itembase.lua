--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


GMRP.Items = GMRP.Items or {}

function GMRP.AddItem( itemtable )

	if !itemtable.IColor then
		itemtable.IColor = Color(255,255,255)
	end

	GMRP.Items[itemtable.ID] =
	{
		Name = itemtable.Name,
		Permit = itemtable.Permit or false,
		Job = itemtable.Job or false,
		IColor = itemtable.IColor,		
		Price = itemtable.Price,
		Info = itemtable.Info,	
		Type = itemtable.Type,
		Energy = itemtable.Energy,
		Model = itemtable.Model,
		Spawn = itemtable.Spawn,
		IUse = itemtable.Use,
		Remove = itemtable.Remove,
		Merchant = itemtable.Merchant or false,
		Enforce_Owner = itemtable.Enforce_Owner,
	}
	
end	


if (!SERVER) then return end

function GMRP.BaseItemSpawn( p, itemtable )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	


	local sitem = ents.Create( "prop_physics" )
	sitem:SetModel( itemtable.Model )
	sitem:SetColor( itemtable.IColor )
	sitem:SetPos( tr.HitPos )
	sitem:Spawn()
	sitem:SetEntOwner(p)
	sitem:SetNW2String("ent_itemid", itemtable.ID)
	sitem:Activate()
	sitem:GetPhysicsObject():Wake()	
	sitem:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
	sitem.ID = itemtable.ID
	sitem.IsItem = true
	sitem.IUse = itemtable.Use
	sitem.Removeable = itemtable.Remove
	sitem.Enforce_Owner = itemtable.Enforce_Owner
	
end


function GMRP.BaseUse( p, itemtable )	
	
	p:AddEnergy( itemtable.Energy )

end


function GMRP.BaseSpecialUse( p, itemtable, type, ent )

	if ( itemtable.UsePermit != nil ) then
	
		if ( p:isFavPermit( itemtable.UsePermit ) ) then
		
			p:Give( type )
			ent:Remove()
			
		else

			p:RedNotify( "Must have a "..itemtable.UsePermit.." permit to use this." )
			
		end
		
	end
	
end


function GMRP.BaseVehicle( p, mdl, ent, script, itemtable )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 130
	trace.filter = p
	local tr = util.TraceLine( trace )	

	local jeep = ents.Create( ent )
	jeep:SetModel( mdl )	
	jeep:SetColor( itemtable.IColor )
	jeep:SetKeyValue( "actionScale", 1 ) 
 	jeep:SetKeyValue( "VehicleLocked", 0 ) 
 	jeep:SetKeyValue( "solid", 6 ) 
	jeep:SetKeyValue( "vehiclescript", script ) 		
	jeep:SetPos( tr.HitPos )
	jeep:Spawn()
	jeep:SetEntOwner(p)
	jeep:SetNW2String("ent_itemid", itemtable.ID)
	jeep:Activate()	
	jeep.ID = itemtable.ID
	jeep.IUse = itemtable.Use	
	jeep.Enforce_Owner = itemtable.Enforce_Owner
	
end	


function GMRP.BaseBook( p, itemtable )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	

	local sbook = ents.Create( "prop_physics" )
	sbook:SetModel( itemtable.Model )
	sbook:SetColor( itemtable.IColor )
	sbook:SetPos( tr.HitPos )
	sbook:Spawn()
	sbook:SetEntOwner(p)
	sbook:SetNW2String("ent_itemid", itemtable.ID)
	sbook:Activate()
	sbook:GetPhysicsObject():Wake()	
	sbook:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
	sbook.ID = itemtable.ID
	sbook.IsItem = true
	sbook.IUse = itemtable.Use
	sbook.Enforce_Owner = itemtable.Enforce_Owner
	
end


function GMRP.BaseBookUse( p, ent )

	if ent.ID != nil then
	
		if GMRP.UseLimit( p ) then --in case if player will use book too many times
	
			p:SendLua( "GMRP.BookMenu( '"..ent.ID.."' )" )
			
		end	

	end

end	


function GMRP.BaseSent( p, class, itemtable )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)	

	local sent = ents.Create( class )
	sent:SetPos( tr.HitPos )
	sent:Spawn()
	sent:Activate()
	sent:SetEntOwner(p)
	sent:SetNW2String("ent_itemid", itemtable.ID)
	sent:GetPhysicsObject():Wake()	
	sent:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
	sent:SetColor( itemtable.IColor )
	sent.ID = itemtable.ID
	sent.IsItem = true
	sent.IUse = itemtable.Use
	sent.Enforce_Owner = itemtable.Enforce_Owner
	
end
	

function GMRP.BaseCreator( p, pt, itemtable )

	local trace = {}
	trace.start = p:EyePos()
	trace.endpos = trace.start + p:GetAimVector() * 85
	trace.filter = p
	local tr = util.TraceLine(trace)
	
	local creator = ents.Create( "prop_physics" )
	creator:SetModel( itemtable.Model )
	creator:SetPos( tr.HitPos )
	creator:Spawn()
	creator:SetEntOwner(p)
	creator:SetNW2String("ent_itemid", itemtable.ID)
	creator:Activate()
	creator:SetMoveType( MOVETYPE_NONE )
	creator:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
	creator.ID = itemtable.ID
	creator.IsItem = true
	creator.Enforce_Owner = itemtable.Enforce_Owner
	local cEntIndex = creator:EntIndex()
	

	creator.IUse = function( p, ent )

		if ( p.plantsTime == nil ) then
		
			p.plantsTime = {}
			
		end	
	
		if ( p.plantsTime[cEntIndex] == nil ) then
		
			p.plantsTime[cEntIndex] = 0
			
		end
		
		if ( CurTime() > p.plantsTime[cEntIndex] ) then
	
			local itemtable2 = GMRP.Items[pt]
			
			local created = ents.Create( "prop_physics" )
			created:SetModel( itemtable2.Model )
			created:SetPos( creator:GetPos() + Vector( 0, 0, 100 ) )
			created:Spawn()
			created:SetEntOwner(p)
			created:SetNW2String("ent_itemid", itemtable2.ID)
			created:Activate()
			created:GetPhysicsObject():Wake()	
			created:EmitSound( "physics/cardboard/cardboard_box_break3.wav" )
			created.ID = pt
			created.IsItem = true
			created.IUse = itemtable2.Use
			created.Removeable = itemtable2.Remove
			created.Enforce_Owner = itemtable2.Enforce_Owner

			p.plantsTime[cEntIndex] = CurTime() + 60
			
		else

			p:RedNotify( "This plant does not have fruits yet, please try again in "..math.Round( p.plantsTime[cEntIndex] - CurTime() ).." seconds." )

		end	

	end
	
end	