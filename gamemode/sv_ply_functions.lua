--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]


local pmeta = FindMetaTable( "Player" )


function pmeta:ApplyPlayerSettings()

	self:Freeze( false )	
	self:SetJumpPower( 160 )	
	self:SetNWBool( "sitting", false )	
	self:SetNWBool( "ishigh", false )	
	self:SetNWInt( "stamina", 100 )	
	self:SetNWInt( "energy", 100 )
	self.hasFlashlight = false
	self.O2 = false
	self.canRun = true
	self.arrested = false
	self.arrester = nil
	
	if self:GetNWBool( "sleeping" ) then
	
		self:SetNWBool( "sleeping", false )
			
		self:UnSpectate()

		if self.sp:IsValid() then

			self:SetPos( self.sp:GetPos() )
			self.sp:Remove()
			
		end
		
		if timer.Exists( self:SteamID64().."SleepTimer" ) then

			timer.Destroy( self:SteamID64().."SleepTimer" )

		end
		
		self:SetHealth( self.oldHealth )
		self:SetNWInt( "energy", self.oldEnergy )

	end	
	
	if self:GetPData( "rpmodel" ) == nil then --if player has joined for a first time and doesn't have RP model set
	
		self:SetPData( "rpmodel", "models/player/odessa.mdl" ) --set the default model to odessa
		self:SetModel( "models/player/odessa.mdl" )
		
	else	
	
		self:SetModel( self:GetPData( "rpmodel" ) )
		
	end

	if self:GetPData( "rpmodelcolor" ) ~= nil then
		local dbcol = self:GetPData( "rpmodelcolor" )
		local col = string.Split(dbcol, ',')
		if #col == 3 then
			self:SetPlayerColor(Vector(col[1]/255,col[2]/255,col[3]/255))
		end
	end
	
	if self:IsOnFire() then
	
		self:Extinguish()
		
	end
	

	self:LoadOut()
	
	
	GAMEMODE:SetPlayerSpeed( self, 150, 300 )	
	
end	


function pmeta:SpawnProtection()

	self:GreenNotify( "Spawn protection activated." )
	self:GodEnable()
	self:SetColor( Color(255, 255, 255, 100 ) )

	timer.Simple( 5, function()
	
		if self:IsValid() && self:Alive() then
		
			self:RedNotify( "Spawn protection deactivated." )
			self:GodDisable()
			self:SetColor( Color(255, 255, 255, 255 ) )
			
		end	
		
	end )	
	
end	

function pmeta:SetJob( jobid )
	local JobTimeout = Configuration["jobchangeint"]
	if !self:Alive() then
		return
	end
	if (!self.Job) then self.Job = GMRP.Jobs['default'] end
	if (!self.Job.LastChange) then self.Job.LastChange = 1 end
	if self.Job.LastChange + JobTimeout > os.time() then
		self:RedNotify("You need to wait "..tostring((self.Job.LastChange + JobTimeout) - os.time()).." more seconds to change job.")
		return
	end
	if !GMRP.JobExists(jobid) then
		jobid = "default"
	end
	self.Job = GMRP.Jobs[jobid]
	self:LoadOut()
	self:SetNWString("gm_rp_jobid", jobid)
	self:GreenNotify("You are now "..self.Job.Title)
	self.Job.LastChange = os.time()
	local effectdata = EffectData()
    effectdata:SetOrigin(self:GetPos() + Vector(0,0,50))
    effectdata:SetMagnitude(3)
    effectdata:SetScale(2)
    effectdata:SetRadius(5)
	util.Effect("Sparks", effectdata)
end

function pmeta:LoadOut()

	self:StripWeapons()

	self:Give( "weapon_physcannon" ) 
	self:Give( "weapon_physgun" ) 
	self:Give( "gmod_camera" ) 
	self:Give( "gmod_rp_hands" )
	self:Give( "gmod_rp_keys" )
	self:Give( "gmod_tool" )

	if (self.Job && self.Job.OnLoadout) then
		self.Job.OnLoadout(self)
	end
end	


function pmeta:RealisticDamage()

	self:EmitSound( "vo/npc/male01/pain0"..math.random( 1, 9 )..".wav" )
	
end

function pmeta:applyPayDay()

	if timer.Exists( self:SteamID64().."PayDayTimer" ) then
	
		timer.Destroy( self:SteamID64().."PayDayTimer" )
	
	end
	
	timer.Create( self:SteamID64().."PayDayTimer", tonumber( Configuration["paydayint"] ), 0, function()
		
		local job = self:GetJob()
		self:GreenNotify( "Pay day! +"..GMRP.FormatMoney(job.Paycheck) )
		self:AddMoney( job.Paycheck )
			
	end )	
	
end	


function pmeta:SetPlayerEnergy( amount )

	self:SetNWInt("energy", tonumber(amount))
	
end	

function pmeta:SendChat(...)
	local chattable = {...}
	net.Start("SBRPPacket")
	net.WriteString("CHAT")
	net.WriteTable(chattable)
	net.Send(self)
end

function pmeta:GlobalChat(chattable)
	if (type(chattable) == "string") then
		chattable = {Color(255,255,255),"[OOC] ", self:GetJobColor(), self:Nick(), Color(255,255,255), ": ", chattable}
	end
	for _,ply in pairs(player.GetAll()) do
		ply:SendChat(unpack(chattable))
	end
end

function pmeta:RPTalk( range, txt, is_action )
	is_action = is_action or false
	for k, v in pairs( ents.FindInSphere( self:GetPos(), range ) ) do
		if v:IsPlayer() && v:Alive() && is_action then
			local msg = {Color(193, 141, 189), "* ", self:Nick(), Color(188, 120, 183)}
			txt = string.gsub(txt, "/me ", " ")
			table.insert(msg, txt)
			v:SendChat(unpack(msg))
		elseif v:IsPlayer() && v:Alive() then
			v:SendChat(Color(132, 172, 244, 255), self:Nick()..': ', Color(157, 179, 219,255), tostring(txt))
		end
		
	end

end	


GMRP.command( "sit", function( p )

	if GMRP.UseLimit( p ) then

		if p:GetNWBool( "sleeping" ) then

			p:RedNotify( "You can not sit while sleeping." )
			return
			
		end	

		if p:GetNWBool( "sitting" ) then
		
			p:SetNWBool( "sitting", false )
			p:SetMoveType( MOVETYPE_WALK )
			p:GreenNotify( "You are standing." )
			
		else
		
			p:SetNWBool( "sitting", true )
			p:SetMoveType( MOVETYPE_NONE )
			p:GreenNotify( "You are sitting." )
			
		end
		
	end	
	
end )


GMRP.command( "sleep", function( p )

	if GMRP.UseLimit( p ) then

		if p:GetNWBool( "sitting" ) then

			p:RedNotify( "You can not sleep while sitting." )
			return
			
		end	
		
		
		if p:Alive() then
		
			if p:GetNWBool( "sleeping" ) then
			
				p:Spawn()
				p:GreenNotify( "You woke up." )
				
			else
			
			
				if ( !p:IsOutside() ) then
				
					p:SetNWBool( "sleeping", true )
					p:Freeze( true )

					p.sp = ents.Create( "prop_ragdoll" )
					p.sp:SetPos( p:GetPos() )
					p.sp:SetAngles( Angle( 0, p:GetAngles().Yaw, 0 ) )
					p.sp:SetModel(p:GetModel())
					p.sp:Spawn()
					p.sp:Activate()
					p.sp:SetVelocity( p:GetVelocity() )
					p.oldHealth = p:Health()
					p.oldEnergy = p:GetNWInt( "energy" )				
					p:StripWeapons()
					p:Spectate( OBS_MODE_CHASE )
					p:SpectateEntity( p.sp )			
					
					timer.Create( p:SteamID64().."SleepTimer", 5, 0, function()
					
						if p.oldHealth < 100 then
					
							p.oldHealth = p.oldHealth + 1
					
						end
						
						if p.oldEnergy > 0 then
						
							p.oldEnergy = p.oldEnergy - 5
							
						end	
						
						if !p:Alive() then
						
							p:Freeze( false )
							p:SetNWBool( "sleeping", false )		
							timer.Destroy( p:SteamID64().."SleepTimer" )	
							
						end	
						
					end )
					
					p:GreenNotify( "You are sleeping." )
					
				else
				
					p:RedNotify( "You can not sleep outside." )
					
				end	
				
			end
		
		end
		
	end	
	
end )


function GM:SetPlayerAnimation( p, weapanim )

	self.BaseClass:SetPlayerAnimation( p, weapanim )
	
	if p:GetNWBool( "sitting" ) then
	
		local seq = p:LookupSequence( "sit" )	
		p:SetPlaybackRate( 1 )	
		p:ResetSequence( seq )
		p:SetCycle( 1 )
		
	end

end	


local unDropable = { "hand", "keys" }

GMRP.command( "dropweapon", function( p )
	
	if GMRP.UseLimit( p ) then
	
		local actWeap = p:GetActiveWeapon()

		if actWeap:IsValid() then
		
			for _, v in pairs( unDropable ) do
		
				if !string.find( actWeap:GetClass(), v ) then
				
					p:DropWeapon( actWeap )
					
				end

			end
			
		end
		
	end	

end	)

GMRP.command( "setjob", function(p, cmd, args)
	if (!args || #args == 0 || #args > 1) then
		return 
	end
	p:SetJob(args[1])
end)


function pmeta:CleanStuff()

	self:cleanEntOwnerShip()
	
	if timer.Exists( self:SteamID64().."PayDayTimer" ) then
	
		timer.Destroy( self:SteamID64().."PayDayTimer" )
		
	end	
	
	if timer.Exists( self:SteamID64().."SleepTimer" ) then
	
		timer.Destroy( self:SteamID64().."SleepTimer" )
		
	end	
	
end	