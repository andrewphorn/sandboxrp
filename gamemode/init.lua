--[[

	GModRP - Real Garry's Mod Roleplay.
			By Slob187.
		www.plane-builders.com

]]
   
--includes
include('shared.lua')
include('sv_ply_functions.lua')
include("configuration.lua")

--lua file download
AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("itembase.lua")

BASE_DIR = "sandboxrp/gamemode/"
file.CreateDir("sandboxrp/inventory")
file.CreateDir("sandboxrp/doors")
file.CreateDir("sandboxrp/rppermits1")
file.CreateDir("sandboxrp/settings")

--base include
for k, v in pairs(file.Find(BASE_DIR.."base/*", "LUA")) do

	include("base/"..v)
	
end


--plugins include
for k, v in pairs(file.Find(BASE_DIR.."sv_plugins/*.lua", "LUA")) do

	include("sv_plugins/"..v)
	
end


--derma download
for k, v in pairs( file.Find( BASE_DIR.."derma/*.lua", "LUA")) do

	AddCSLuaFile("derma/"..v)
	
end


--vgui download
for k, v in pairs( file.Find( BASE_DIR.."vgui/*.lua", "LUA")) do

	AddCSLuaFile("vgui/"..v)
	
end

-- set up nwstring for gamemode packets
util.AddNetworkString("SBRPPacket")

Radiostations = {}

function GMRP.AddMaterial( f )

	resource.AddFile( "materials/"..f..".vmt" )
	resource.AddFile( "materials/"..f..".vtf" )
	
end	

function GMRP.RegisterPNG( f )
	resource.AddFile( "materials/sandboxrp/icons/"..f..".png" )
end


GMRP.RegisterPNG( "iheart" )
GMRP.RegisterPNG( "ifood" )
GMRP.RegisterPNG( "ienergy" )
GMRP.RegisterPNG( "ibank" )


--settings
if Configuration["realistic"] == 1 then

	game.ConsoleCommand( "sbox_godmode 0\n" )
	game.ConsoleCommand( "sbox_noclip 0\n" )
	
end	

game.ConsoleCommand( "sv_playerpickupallowed 0\n" )


if Configuration["economy"] == 1 then

	timer.Create( "GModRPEconomy", Configuration["economyinterval"], 0, function()
	
		for k, v in pairs( GMRP.Items ) do
		
			local newPrice = math.Round( v.Price + math.random( -10, 10 ) )
			
			if ( newPrice > 0 ) then
			
				v.Price = newPrice
				
				RunLuaAll( "GMRP.Items['"..k.."'].Price = "..newPrice )
				
			end

		end
		
	end )
	
end

	
function GM:PlayerSpawn( ply )

	if !ply:GetNWBool( "sleeping" ) then
	
		if game.GetMap() == "rp_oviscity_gmc4" then
	
			GMRP.OvisCitySpawns( ply )
		
		end	
		
		ply:SpawnProtection()

	end
	
	ply:ApplyPlayerSettings()
	
	ply:UnSpectate()
	
end



function GM:PlayerHurt( ply )

	self.BaseClass:PlayerHurt( ply )
	ply:RealisticDamage()
	
end	



function GMRP.SetModel( ply, command, args )

	for k, v in pairs( citizenmodels ) do
	
		if tostring( args[1] ) == v then
		
			if GMRP.UseLimit( ply ) then
				ply:SetPData( "rpmodel", args[1] )
				ply:SetModel( args[1] )
				ply:GreenNotify( "Player model changed to "..tostring( args[1] )..". This model will stay after rejoining." )	
			end	
			
		end
		
	end
	
end

GMRP.command( "setmodel", GMRP.SetModel )

function GMRP.SetModelColor( ply, command, args )
	if #args ~= 3 then return nil end
	local col = args[1]..","..args[2]..","..args[3]
	ply:SetPData( "rpmodelcolor" , col)
	ply:SetPlayerColor(Vector(args[1]/255, args[2]/255, args[3]/255))
end

GMRP.command( "setmodelcolor", GMRP.SetModelColor )


function GM:PlayerDeathSound(ply)

	ply:EmitSound("vo/npc/Barney/ba_ohshit03.wav") --say 'oh shit' when player dies
	return true --disable the default beep sound	
	
end


function GM:DoPlayerDeath( p, attacker, dmginfo )

	p:RedNotify( "You got knocked out and lost "..CUR..Configuration["knockouttax"].."." )
	p:TakeMoney( Configuration["knockouttax"] )

	p:CreateRagdoll()
	
end


function GM:ShowTeam( p )

	p:ConCommand( "gm_rp_shopmenu" )
	
end


function GM:ShowHelp(ply)

	ply:ConCommand("gm_rp_motd")
	return false
	
end


function GM:PlayerInitialSpawn( ply )

	ply:SetNW2Bool("phys_limited", tobool(Configuration['limitedphysgun']))
	
	self.BaseClass:PlayerInitialSpawn( ply )

	ply:SetJob( "default" )

	ply:applyPayDay()
	
	ply:SetupAccount()

	ply:CorrectMoney()

	ply:ConCommand( "gm_rp_motd" )

	GMRP.SendInventory( ply )
	
	GMRP.SendPermits( ply )

	for k, v in pairs( Radiostations ) do
	
		ply:SendLua("table.insert( Radiostations, '"..tostring( v ).."' )")
		
	end	
	
	if ( ply:Nick() == "UNOWNABLE" ) then

		ply:Kick( "Please change your name and rejoin." )
		
	end	

end


function GM:PlayerDisconnected( ply )

	self.BaseClass:PlayerDisconnected( ply )
	
	ply:CleanStuff()
	
end


local NextFireSpreadTime = 0

function GM:Think()

	for _, ply in pairs( player.GetAll() ) do	
	
		if ply:Alive() then
	
			ply:staminaCheck()
			
			ply:CheckHunger()		
		
			if ( ply.arrested ) then
			
				if ply.arrester:IsValid() then
				
					if ply.arrester:Alive() then
				
						ply:SetVelocity( ( ply.arrester:GetPos() - ply:GetPos() ):Normalize() * 100 )
						
					end	
					
				end

			end
		
			if ply:GetNWBool( "ishigh" ) then
			
				if ( ply:GetVelocity():Length() > 0 ) then

					ply:SetVelocity( Vector( math.random( -50, 50 ), math.random( -50, 50 ), 0 ) )
					
				end	
				
			end	
			
			if ply.NextO2Think == nil then
			
				ply.NextO2Think = 0
				
			end	
		
			if CurTime() > ply.NextO2Think then
			
				if !ply:Alive() then return end		
				
				if !ply.O2 then
				
					if ply:WaterLevel() > 2 then
					
						ply:TakeDamage( 5, ply, ply )	
						
					end
					
				end	
				
				ply.NextO2Think = CurTime() + 1
				
			end
			
		end	
		
	end

	
	if CurTime() > NextFireSpreadTime then
	
		for k, v in pairs( ents.GetAll() ) do
		
			if v:GetClass() == "prop_physics" || v:IsPlayer() then
		
				if v:IsOnFire() then
				
					v:SetColor( Color(0, 0, 0, 255 ) )
					
					for _, ent in pairs( ents.FindInSphere( v:GetPos(), math.random( 30, 100 ) ) ) do
					
						if ( !ent:IsOnFire() ) then
					
							if ent:IsPlayer() && ent:Alive() then
							
								ent:Ignite( 30, 0 )
								ent:SetColor( Color(100, 100, 100, 255 ) )
		
							elseif ent:GetClass() == "prop_physics" then
							
								ent:Ignite( 300, 0 )
								ent:SetColor( Color(0, 0, 0, 255 ) )
								
							end	
							
						end	
						
					end
					
				end
				
			end	
			
		end
		
		NextFireSpreadTime = CurTime() + 3
		
	end	
	
end	


function GM:PlayerShouldTakeDamage( victim, attacker )

	if attacker:IsValid() then
	
		local attClass = attacker:GetClass()
	
		if attClass == "prop_physics" then --disable the prop damage - no more prop killing
		
			return false
	
		elseif attacker:IsPlayer() then
		
			if attacker:GetActiveWeapon():GetClass() == "weapon_stunstick" then
				
				victim:SetVelocity( attacker:GetAimVector():GetNormalized() * 1000 )
				victim:TakeDamage( 5, attacker, attacker:GetActiveWeapon() )		
				
				return false		
				
			end			
			
		end	
		
	end
	
	return true
	
end


function GM:GravGunPunt( userid, target )

	DropEntityIfHeld( target )
	target:GetPhysicsObject():ApplyForceCenter(userid:GetAngles():Forward() * 5000)
	
	return false
	
end	


concommand.Add( "gm_spare1", function( p )

	p:ConCommand( "gm_showspare1" )
	
end )	


concommand.Add( "gm_spare2", function( p )

	p:ConCommand( "gm_showspare2" )
	
end )
	
	
	
function GM:ShowSpare1( p )

	p:ConCommand( "gm_rp_playermenu" )
	
end


function RunLuaAll( lua )

	for k, v in pairs( player.GetAll() ) do
	
		v:SendLua( lua )
		
	end

end


function GM:KeyPress( ply, key )

	self.BaseClass:KeyPress( ply, key )

	local trace = {}
	trace.start = ply:EyePos()
	trace.endpos = trace.start + ply:GetAimVector() * 85
	trace.filter = ply
	local tr = util.TraceLine( trace )	
	
	if !tr.Entity:IsValid() then return end	
	
	if key == IN_USE && !ply:KeyDown( IN_SPEED ) then
		
		if tr.Entity.Money then
		
			ply:GreenNotify( "+"..CUR..tr.Entity.Amount )
			ply:AddMoney(tr.Entity.Amount )
			tr.Entity:Remove()
			
		end	
		
		if tr.Entity:GetNWString( "permit" ) != "" then
		
			if tr.Entity.Owner == ply then
		
				tr.Entity:Remove()
				ply.Permits = ply.Permits - 1
				
			end	
			
		end	
		
		if !tr.Entity.IsItem then return end
		
		tr.Entity.IUse( ply, tr.Entity )
		
		if tr.Entity.Removeable then
		
			tr.Entity:Remove()
			
		end	

	elseif ( key == IN_RELOAD ) then
	
		if !ply:InVehicle() then
	
			if tr.Entity.IsItem or ( string.find( tr.Entity:GetClass(), "vehicle" ) and tr.Entity:IsOwnedBy(ply) ) then
			
				if string.find( tr.Entity:GetClass(), "vehicle" ) then
				
					ply.alreadyOwned = ply.alreadyOwned - 1
					
				end	
				
				if !tr.Entity.IsItem or (!tr.Entity.Enforce_Owner or (tr.Entity.Enforce_Owner and tr.Entity:IsOwnedBy(ply))) then
					--if it's a radiostation, delete the radiostation's name from radiostation list
					GMRP.DeleteRadiostation( tr.Entity )				
				
					GMRP.AddToInentory( ply, tr.Entity.ID )	
					tr.Entity:Remove()
				end
				
			end
			
		end	

	end

end	


function GM:GetFallDamage( ply, fspeed )
 
	return ( fspeed / 8 )
 
end


function GM:CanPlayerSuicide( p )

	if p.arrested then
	
		p:PrintMessage( HUD_PRINTCONSOLE, "You can not suicide while handcuffed." )
		return false
		
	end	
	
	return true
	
end


function GM:PlayerSwitchFlashlight( ply, SwitchOn )
	
	return ply.hasFlashlight || !SwitchOn

end	


function GM:PlayerCanHearPlayersVoice( l, t )

	for k, v in pairs( ents.FindInSphere( t:GetPos(), Configuration["talkrange"] ) ) do
	
		if v == l then
		
			return true
			
		end

	end
	
	return false
	
end


function GM:EntityRemoved( ent )

	--GMRP.DeleteRadiostation( ent )
	
end	

net.Receive("SBRPPacket", function(len, ply) 
	local pname = net.ReadString()
	if pname == "CHAT" then
		local chattext = net.ReadString()
		chattext = GAMEMODE:PlayerSay(ply, chattext)
		if chattext ~= "" then
			ply:GlobalChat(chattext)
		end
	end
end)

hook.Add( "PlayerSpawnSWEP", "WeaponSpawnBlock", function( ply, class, info )
	if not ply:IsAdmin() then
		return false
	end
end )

hook.Add("PlayerGiveSWEP", "WeaponGiveBlock", function( ply, class, swep )
	if ( not ply:IsAdmin() ) then
		return false
	end
end )